import React, { Component } from 'react'
import { Field, reduxForm } from 'redux-form'
import { connect } from 'react-redux'
import { authLogin } from '../actions/authentication'
import LogoIcon from '../../assets/images/logo-icon.png'
import PersonIcon from '@material-ui/icons/Person'
import VpnKeyIcon from '@material-ui/icons/VpnKey'
import PreloaderIcon from '../../assets/images/preloader.svg'

class LoginView extends Component {
    constructor (props) {
        super(props)
        this.state = {
            isLoading: false
        }
    }

    submit = (values) => {
        this.setState({
            isLoading: true
        }, () => {
            this.props.authLogin(values, this.props.history).finally(() => {
                this.setState({ isLoading: false })
            })
        })
    }

    render () {
        const {
            pristine,
            submitting,
            authMessage,
            handleSubmit
        } = this.props
        const {
            isLoading
        } = this.state
        return (
            <div className='auth-wrapper d-flex no-block justify-content-center align-items-center'>
                <div className='auth-box'>
                    <div id='loginform'>
                        <div className='logo'>
                            <span className='db'>
                                <img src={LogoIcon} alt='logo' />
                            </span>
                            <h5 className='font-medium m-b-20'>เข้าสู่ระบบ</h5>
                        </div>
                        {authMessage && <div className='text-danger'>{authMessage.message}</div>}
                        <div className='row'>
                            <div className='col-12'>
                                <form onSubmit={handleSubmit(this.submit)} className='form form-horizontal m-t-20' id='loginform'>
                                    <div className='input-group mb-3'>
                                        <div className='input-group-prepend'>
                                            <span className='input-group-text' id='basic-addon1'>
                                                <PersonIcon />
                                            </span>
                                        </div>
                                        <Field name='username' component='input' type='text' placeholder='Username' className='form-control form-control-lg' />
                                    </div>
                                    <div className='input-group mb-3'>
                                        <div className='input-group-prepend'>
                                            <span className='input-group-text' id='basic-addon1'>
                                                <VpnKeyIcon />
                                            </span>
                                        </div>
                                        <Field name='password' component='input' type='password' placeholder='Password' className='form-control form-control-lg' />
                                    </div>
                                    <div className='form-group text-center'>
                                        {isLoading
                                            ? <PreloaderIcon color='#188fa7' width={64} height={64} />
                                            : <button type='submit' className='btn btn-block btn-lg btn-info' disabled={pristine || submitting}>Log In</button>
                                        }
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        authMessage: state.message.authMessage
    }
}

const reduxFormLoginView = reduxForm({
    form: 'login'
})(LoginView)

export default connect(mapStateToProps, { authLogin })(reduxFormLoginView)
