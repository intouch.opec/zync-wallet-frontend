import React, { Component } from 'react'
import { Link, withRouter } from 'react-router-dom'
import { API_CUSTOMER_URL } from '../../utils/urls'
import client from '../../utils/client'
import Swal from 'sweetalert2'
import CustomerForm from '../../components/form-layouts/form-layout-customer'

class CustomerCreateView extends Component {
    handleSubmitCustomer = (value, reset) => {
        const {
            history
        } = this.props

        client.post(API_CUSTOMER_URL, value).then(res => {
            reset()
            Swal.fire({
                type: 'success',
                title: 'บันทึกข้อมูลสำเร็จ',
                showConfirmButton: false,
                timer: 1500
            })
            history.replace({ pathname: `/customers/${res.data.id}` })
        }).catch(err => {
            if (err.response.data.username) {
                Swal.fire({
                    type: 'error',
                    title: 'username ซ้ำ กรุณาเปลี่ยน username',
                    showConfirmButton: false,
                    timer: 1500
                })
            }
        })
    }

    render () {
        return (
            <div className='page-wrapper'>
                <div className='page-breadcrumb'>
                    <h1 className='page-title'>เพิ่มลูกค้า</h1>
                    <ol className='breadcrumb'>
                        <li className='breadcrumb-item'>
                            <Link to='/'>หน้าแรก</Link>
                        </li>
                        <li className='breadcrumb-item'>
                            <Link to='/customers'>ลูกค้า</Link>
                        </li>
                        <li className='breadcrumb-item active'>เพิ่มลูกค้า</li>
                    </ol>
                </div>
                <div className='container-fluid'>
                    <div className='card m-4'>
                        <div className='card-body'>
                            <h4 className='card-title'>เพิ่มลูกค้า</h4>
                            <CustomerForm handleSubmitCustomer={this.handleSubmitCustomer} />
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}

export default withRouter(CustomerCreateView)
