import React, { Component } from 'react'
import { Link, withRouter } from 'react-router-dom'
import { API_CUSTOMER_DETAIL_URL, API_CUSTOMER_URL } from '../../utils/urls'
import client from '../../utils/client'
import EditIcon from '@material-ui/icons/Edit'
import AddIcon from '@material-ui/icons/Add'
import PageviewIcon from '@material-ui/icons/Pageview'
import Swal from 'sweetalert2'
import DeleteIcon from '@material-ui/icons/Delete'
import Table from '@material-ui/core/Table'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import TableCell from '@material-ui/core/TableCell'
import TableBody from '@material-ui/core/TableBody'
import Paper from '@material-ui/core/Paper'
import { getNoDataTableBody } from '../../components/table'
import { Input, UncontrolledTooltip } from 'reactstrap'
import _ from 'lodash'

const options = [
    { name: 'username', key: 'username' },
    { name: 'ชื่อ-นามสกุล', key: 'fullname' },
    { name: 'เบอร์โทร', key: 'telephone' },
    { name: 'Line_id', key: 'line_id' },
    { name: 'email', key: 'email' }
]

class CustomerListView extends Component {
    constructor (props) {
        super(props)
        this.state = {
            customers: [],
            key: 'username',
            valueFilter: ''
        }
    }

    componentDidMount () {
        this.fetchCustomers()
    }

    fetchCustomers = () => {
        client.get(API_CUSTOMER_URL).then(res => {
            this.setState({
                customers: res.data
            })
        })
    }

    renderOptions = options => {
        return options.map(option => (
            <option value={option.key}>{option.name}</option>
        ))
    }

    getTableBody = () => {
        let customers = this.state.customers

        if (this.state.valueFilter.length > 0) {
            const filterValue = this.state.valueFilter.trim().toLowerCase()
            customers = this.state.customers.filter(item => {
                return item[this.state.key].toLowerCase().includes(filterValue)
            })
        }

        if (customers.length === 0) {
            return getNoDataTableBody(6)
        }

        return customers.map(row => {
            return (
                <TableRow key={row.id}>
                    <TableCell>{row.username}</TableCell>
                    <TableCell>{row.fullname}</TableCell>
                    <TableCell>{row.telephone}</TableCell>
                    <TableCell>{row.line_id}</TableCell>
                    <TableCell>{row.customer_level.name}</TableCell>

                    <TableCell className='text-right'>
                        <Link id={`btnView${row.id}`} to={`/customers/${row.id}`} className='btn btn-icon'>
                            <PageviewIcon />
                        </Link>
                        <UncontrolledTooltip placement='auto' target={`btnView${row.id}`}>
                            ดูข้อมูล
                        </UncontrolledTooltip>
                        <Link id={`btnEdit${row.id}`} to={`/customers/${row.id}/edit`} className='btn btn-icon'>
                            <EditIcon />
                        </Link>
                        <UncontrolledTooltip placement='auto' target={`btnEdit${row.id}`}>
                            แก้ไขข้อมูล
                        </UncontrolledTooltip>
                        <button id={`btnRejected${row.id}`} className='btn btn-icon' onClick={e => this.removeCustomer(row)}>
                            <DeleteIcon />
                        </button>
                        <UncontrolledTooltip placement='auto' target={`btnRejected${row.id}`}>
                            ลบข้อมูล
                        </UncontrolledTooltip>
                    </TableCell>
                </TableRow>
            )
        })
    }

    handleOnChange = e => {
        this.setState({ [e.target.name]: e.target.value })
    }

    removeCustomer = (row) => {
        Swal.fire({
            title: 'ยืนยันการลบข้อมูล',
            type: 'question',
            showCancelButton: true,
            showCloseButton: true
        }).then(result => {
            if (result.value) {
                client.delete(API_CUSTOMER_DETAIL_URL.replace('customerId', row.id)).then(res => {
                    const customers = _.filter(this.state.customers, obj => obj.id !== row.id)
                    this.setState({ customers }, () => {
                        Swal.fire({
                            type: 'success',
                            title: 'ลบข้อมูลสำเร็จ',
                            showConfirmButton: false,
                            timer: 1500
                        })
                    })
                })
            }
        })
    }

    render () {
        return (
            <div className='page-wrapper'>
                <div className='page-breadcrumb'>
                    <h1 className='page-title'>ลูกค้า</h1>
                    <ol className='breadcrumb'>
                        <li className='breadcrumb-item'>
                            <Link to='/'>หน้าแรก</Link>
                        </li>
                        <li className='breadcrumb-item active'>ลูกค้า</li>
                    </ol>
                </div>

                <div className='container-fluid'>
                    <Paper className='p-4'>
                        <div className='d-flex align-items-center'>
                            <h4 className='card-title'>รายชื่อลูกค้า</h4>
                            <div className='ml-auto'>
                                <Link to='/customers/create' className='btn btn-info' >
                                    <AddIcon /> เพิ่มลูกค้า
                                </Link>
                            </div>
                        </div>
                        <div className='row'>
                            <div className='col-2'>
                                <Input onChange={this.handleOnChange} type='select' name='key'>
                                    {this.renderOptions(options)}
                                </Input>
                            </div>
                            <div className='col-3'>
                                <Input name='valueFilter' onChange={this.handleOnChange} />
                            </div>
                        </div>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell>ชื่อผู้ใช้</TableCell>
                                    <TableCell>ชื่อ</TableCell>
                                    <TableCell>เบอร์โทร</TableCell>
                                    <TableCell>Line ID</TableCell>
                                    <TableCell>ระดับสมาชิก</TableCell>
                                    <TableCell />
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {this.getTableBody()}
                            </TableBody>
                        </Table>
                    </Paper>
                </div>
            </div>)
    }
}

export default withRouter(CustomerListView)
