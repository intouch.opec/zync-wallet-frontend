import React, { Component } from 'react'
import { Link, withRouter } from 'react-router-dom'
import { API_CUSTOMER_DETAIL_URL } from '../../utils/urls'
import client from '../../utils/client'
import CustomerForm from '../../components/form-layouts/form-layout-customer'
import Swal from 'sweetalert2'

class CustomerEditView extends Component {
    constructor (props) {
        super(props)
        this.state = {
            customer: null
        }
    }

    componentDidMount () {
        this.fetchCustomer()
    }

    fetchCustomer = () => {
        const {
            match: { params }
        } = this.props
        client.get(API_CUSTOMER_DETAIL_URL.replace('customerId', params.id)).then(res => {
            this.setState({ customer: res.data })
        })
    }

    handleSubmitCustomer = (value, reset) => {
        const {
            match: { params }
        } = this.props
        client.put(API_CUSTOMER_DETAIL_URL.replace('customerId', params.id), value).then(res => {
            this.setState({ customer: value })
            Swal.fire({
                type: 'success',
                title: 'บันทึกข้อมูลสำเร็จ',
                showConfirmButton: false,
                timer: 1500
            })
        })
    }

    render () {
        const {
            match: { params }
        } = this.props

        const {
            customer
        } = this.state
        const initialValues = customer || {}
        const customerUsername = customer ? customer.username : '-'
        return (
            <div className='page-wrapper'>
                <div className='page-breadcrumb'>
                    <h1 className='page-title'>แก้ไขข้อมูล {customerUsername}</h1>
                    <ol className='breadcrumb'>
                        <li className='breadcrumb-item'>
                            <Link to='/'>หน้าแรก</Link>
                        </li>
                        <li className='breadcrumb-item'>
                            <Link to='/customers'>ลูกค้า</Link>
                        </li>
                        <li className='breadcrumb-item'>
                            <Link to={`/customers/${params.id}`}>{customerUsername}</Link>
                        </li>
                        <li className='breadcrumb-item active'>แก้ไขข้อมูล</li>
                    </ol>
                </div>
                <div className='container-fluid'>
                    <div className='card m-4'>
                        <div className='card-body'>
                            <CustomerForm initialValues={initialValues} handleSubmitCustomer={this.handleSubmitCustomer} usernameRequired />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default withRouter(CustomerEditView)
