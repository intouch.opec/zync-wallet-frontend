import React, { Component } from 'react'
import { Link, withRouter } from 'react-router-dom'
import client from '../../utils/client'
import Swal from 'sweetalert2'
import { API_PROMOTION_DETAIL_URL } from '../../utils/urls'
import FormPromotion from '../../components/form-layouts/form-layout-promotion'

class PromotionEditView extends Component {
    constructor (props) {
        super(props)
        this.state = {
            promotion: {}
        }
    }

    componentDidMount () {
        this.fetchPromotion()
    }

    fetchPromotion = () => {
        const { match: { params } } = this.props
        client.get(API_PROMOTION_DETAIL_URL.replace('promotionId', params.id)).then(res => {
            this.setState({ promotion: res.data })
        })
    }

    onFormSubmit = (value, reset) => {
        const {
            match: { params }
        } = this.props
        client.put(API_PROMOTION_DETAIL_URL.replace('promotionId', params.id), value).then(res => {
            this.setState({ promotion: res.data })
            Swal.fire({
                type: 'success',
                title: 'บันทึกข้อมูลสำเร็จ',
                showConfirmButton: false,
                timer: 1500
            })
        })
    }

    render () {
        const {
            promotion
        } = this.state
        return (
            <div className='page-wrapper'>
                <div className='page-breadcrumb'>
                    <h1 className='page-title'>แก้ไขโปรโมชั่น</h1>
                    <ol className='breadcrumb'>
                        <li className='breadcrumb-item'>
                            <Link to='/'>หน้าแรก</Link>
                        </li>
                        <li className='breadcrumb-item'>
                            <Link to='/promotions'>โปรโมชั่น</Link>
                        </li>
                        <li className='breadcrumb-item active'>แก้ไข</li>
                    </ol>
                </div>
                <div className='container-fluid'>
                    <div className='card m-4'>
                        <div className='card-body'>
                            <FormPromotion initialValues={promotion} onFormSubmit={this.onFormSubmit} />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default withRouter(PromotionEditView)
