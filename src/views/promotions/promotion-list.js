import React, { Component } from 'react'
import { Link, withRouter } from 'react-router-dom'
import {
    API_PROMOTION_DETAIL_URL, API_PROMOTION_URL
} from '../../utils/urls'
import client from '../../utils/client'
import EditIcon from '@material-ui/icons/Edit'
import AddIcon from '@material-ui/icons/Add'
import Swal from 'sweetalert2'
import DeleteIcon from '@material-ui/icons/Delete'
import Table from '@material-ui/core/Table'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import TableCell from '@material-ui/core/TableCell'
import TableBody from '@material-ui/core/TableBody'
import Paper from '@material-ui/core/Paper'
import { getNoDataTableBody } from '../../components/table'
import { UncontrolledTooltip } from 'reactstrap'
import _ from 'lodash'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import Switch from '@material-ui/core/Switch'

class PromotionListView extends Component {
    constructor (props) {
        super(props)
        this.state = {
            promotions: []
        }
    }

    componentDidMount () {
        this.fetchPromotions()
    }

    fetchPromotions = () => {
        client.get(API_PROMOTION_URL).then(res => {
            this.setState({
                promotions: res.data
            })
        })
    }

    toggleIsActiveSwitch = (row) => {
        const isActive = !row.is_active
        client.put(API_PROMOTION_DETAIL_URL.replace('promotionId', row.id), { is_active: isActive }).then(res => {
            const promotions = this.state.promotions.map(obj => {
                if (obj.id === row.id) obj.is_active = isActive
                return obj
            })
            this.setState({ promotions })
        })
    }

    removePromotion = (row) => {
        Swal.fire({
            title: 'ยืนยันการลบข้อมูล',
            type: 'question',
            showCancelButton: true,
            showCloseButton: true
        }).then(result => {
            if (result.value) {
                client.delete(API_PROMOTION_DETAIL_URL.replace('promotionId', row.id)).then(res => {
                    const promotions = _.filter(this.state.promotions, obj => obj.id !== row.id)
                    this.setState({ promotions }, () => {
                        Swal.fire({
                            type: 'success',
                            title: 'ลบข้อมูลสำเร็จ',
                            showConfirmButton: false,
                            timer: 1500
                        })
                    })
                })
            }
        })
    }

    getTableBody = () => {
        const {
            promotions
        } = this.state
        if (promotions.length === 0) {
            return getNoDataTableBody(2)
        }
        return promotions.map(row => {
            return (
                <TableRow key={row.id}>
                    <TableCell>{row.name}</TableCell>
                    <TableCell className='text-right'>
                        <FormControlLabel control={
                            <Switch color='primary' value={row.id} checked={row.is_active} onChange={e => this.toggleIsActiveSwitch(row)} />
                        } label='ใช้งาน' className='mb-0' />
                        <Link id={`btnEdit${row.id}`} to={`/promotions/${row.id}`} className='btn btn-icon'>
                            <EditIcon />
                        </Link>
                        <UncontrolledTooltip placement='auto' target={`btnEdit${row.id}`}>แก้ไขข้อมูล</UncontrolledTooltip>
                        <button id={`btnRejected${row.id}`} className='btn btn-icon' onClick={e => this.removePromotion(row)}>
                            <DeleteIcon />
                        </button>
                        <UncontrolledTooltip placement='auto' target={`btnRejected${row.id}`}>ลบข้อมูล</UncontrolledTooltip>
                    </TableCell>
                </TableRow>
            )
        })
    }

    render () {
        return (
            <div className='page-wrapper'>
                <div className='page-breadcrumb'>
                    <h1 className='page-title'>โปรโมชั่น</h1>
                    <ol className='breadcrumb'>
                        <li className='breadcrumb-item'>
                            <Link to='/'>หน้าแรก</Link>
                        </li>
                        <li className='breadcrumb-item active'>โปรโมชั่น</li>
                    </ol>
                </div>

                <div className='container-fluid'>
                    <Paper className='p-4'>
                        <div className='d-flex align-items-center'>
                            <h4 className='card-title'>รายการโปรโมชั่น</h4>
                            <div className='ml-auto'>
                                <Link to='/promotions/create' className='btn btn-info' >
                                    <AddIcon />เพิ่มโปรโมชั่น
                                </Link>
                            </div>
                        </div>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell>โปรโมชั่น</TableCell>
                                    <TableCell />
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {this.getTableBody()}
                            </TableBody>
                        </Table>
                    </Paper>
                </div>
            </div>)
    }
}

export default withRouter(PromotionListView)
