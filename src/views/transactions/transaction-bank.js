import React, { Component } from 'react'
import { Link, withRouter } from 'react-router-dom'
import _ from 'lodash'

import client from '../../utils/client'
import {
    API_SYSTEM_BANK_ACCOUNT_URL,
    API_TRANSACTION_BANK_DETAIL_URL,
    API_TRANSACTION_BANK_URL
} from '../../utils/urls'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import Switch from '@material-ui/core/Switch'
import CancelIcon from '@material-ui/icons/Cancel'
import DeleteIcon from '@material-ui/icons/Delete'
import VisibilityIcon from '@material-ui/icons/Visibility'
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff'
import PageviewIcon from '@material-ui/icons/Pageview'
import Table from '@material-ui/core/Table'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import TableCell from '@material-ui/core/TableCell'
import TableBody from '@material-ui/core/TableBody'
import Paper from '@material-ui/core/Paper'
import { getBankIcon, getDateFormat, getTransactionState, thousandFormat } from '../../utils/helpers'
import { UncontrolledTooltip } from 'reactstrap'
import Swal from 'sweetalert2'
import { getNoDataTableBody } from '../../components/table'
import { STATE_DEPOSIT } from '../../utils'
import PusherService from '../../utils/pusher'
import ModalTransactionBankDetail from '../../components/modals/modal-transaction-bank-detail'

class BankListView extends Component {
    constructor (props) {
        super(props)
        this.state = {
            transactionBanks: [],
            selectedSwitches: [],
            isModalOpen: false,
            transactionBank: null
        }
    }

    componentDidMount () {
        this.fetchTransactionBanks()
        const channelTransactionBanks = PusherService.getInstance().subscribe('transaction-bank')
        channelTransactionBanks.bind('new', (data) => {
            this.setState({
                transactionBanks: [ ...this.state.transactionBanks, data ]
            })
        }).bind('update', (data) => {
            const transactionBanks = this.state.transactionBanks.map(obj => obj.id === data.id ? data : obj)
            this.setState({ transactionBanks })
        }).bind('remove', (data) => {
            const transactionBanks = _.filter(this.state.transactionBanks, obj => obj.id !== data.id)
            this.setState({ transactionBanks })
        })
    }

    fetchTransactionBanks = () => {
        client.get(API_TRANSACTION_BANK_URL).then(res => {
            this.setState({ transactionBanks: res.data })
        })
    }

    fetchTransactionBankDetail = (transactionBankId) => {
        client.get(API_TRANSACTION_BANK_DETAIL_URL.replace('transactionBankId', transactionBankId)).then(res => {
            this.setState({
                isModalOpen: true,
                transactionBank: res.data
            })
        })
    }

    updateTransactionBankStatus = (transactionBankId, data) => {
        return client.post(API_TRANSACTION_BANK_DETAIL_URL.replace('transactionBankId', transactionBankId), data)
    }

    toggleModal = () => {
        this.setState({
            isModalOpen: !this.state.isModalOpen,
            transactionBank: null
        })
    }

    toggleApprovedSwitch = (row) => {
        const transactionBankId = row.id
        this.setState({
            selectedSwitches: [
                transactionBankId,
                ...this.state.selectedSwitches
            ]
        }, () => {
            if (row.state === STATE_DEPOSIT) {
                this.updateTransactionBankStatus(transactionBankId, { is_approved: true }).then(res => {
                    Swal.fire({
                        title: 'อนุมัติรายการสำเร็จ',
                        type: 'success',
                        showConfirmButton: false,
                        timer: 1500
                    })
                })
            } else {
                client.get(API_SYSTEM_BANK_ACCOUNT_URL, { params: { is_active: true } }).then(res => {
                    let inputOptions = {}
                    _.each(res.data, item => {
                        inputOptions[item.id] = item.name
                    })
                    Swal.fire({
                        title: 'เลือกบัญชีทำรายการ',
                        text: 'เลือกบัญชีที่ใช้สำหรับการทำรายการ',
                        type: 'warning',
                        input: 'select',
                        inputOptions: inputOptions,
                        inputPlaceholder: '--- กรุณาเลือก ---',
                        showCancelButton: true,
                        confirmButtonText: 'ยืนยัน',
                        cancelButtonText: 'ยกเลิก',
                        inputValidator: (value) => {
                            if (!value) return 'จำเป็นต้องระบุ'
                        }
                    }).then((result) => {
                        if (result.value) {
                            this.updateTransactionBankStatus(transactionBankId, {
                                is_approved: true,
                                system_bank_account_id: result.value
                            }).then(res => {
                                Swal.fire({
                                    title: 'อนุมัติรายการสำเร็จ',
                                    type: 'success',
                                    showConfirmButton: false,
                                    timer: 1500
                                })
                            })
                        }
                    })
                })
            }
            this.setState({
                selectedSwitches: this.state.selectedSwitches.filter(value => value !== transactionBankId)
            })
        })
    }

    onClickInProgressButton = (transactionBankId) => {
        client.put(API_TRANSACTION_BANK_DETAIL_URL.replace('transactionBankId', transactionBankId), { action: 'updateProgress' })
    }

    onClickRejectedButton = (row) => {
        const transactionBankId = row.id
        Swal.fire({
            title: 'คุณต้องการที่จะยกเลิกรายการนี้หรือไม่',
            text: 'ข้อมูลนี้ไม่ถูกลบ แต่เป็นการยกเลิกรายการเท่านั้น',
            type: 'warning',
            input: 'text',
            showCancelButton: true,
            confirmButtonText: 'ยืนยัน',
            cancelButtonText: 'ยกเลิก',
            inputValidator: (value) => {
                if (!value) return 'จำเป็นต้องระบุ'
            }
        }).then((result) => {
            if (result.value) {
                this.updateTransactionBankStatus(transactionBankId, {
                    is_approved: false,
                    remark: result.value
                }).then(res => {
                    Swal.fire({
                        title: 'ยกเลิกรายการสำเร็จ',
                        type: 'success',
                        showConfirmButton: false,
                        timer: 1500
                    })
                })
            }
        })
    }

    onClickDeleteButton = (row) => {
        Swal.fire({
            title: 'ยืนยันการลบข้อมูล',
            type: 'question',
            showCancelButton: true,
            showCloseButton: true
        }).then(result => {
            if (result.value) {
                client.delete(API_TRANSACTION_BANK_DETAIL_URL.replace('transactionBankId', row.id)).then(res => {
                    Swal.fire({
                        type: 'success',
                        title: 'ลบข้อมูลสำเร็จ',
                        showConfirmButton: false,
                        timer: 1500
                    })
                })
            }
        })
    }

    getTableCellBank = (row) => {
        if (row.state === STATE_DEPOSIT) {
            return (
                <TableCell>
                    {getBankIcon(row.bank_account.bank.code)} {row.bank_account.name} - {row.bank_account.account_no}
                </TableCell>
            )
        } else {
            return (
                <TableCell>
                    {getBankIcon(row.bank_account.bank.code)} {row.bank_account.bank_no}
                </TableCell>
            )
        }
    }

    getInProgressIcon = (row) => {
        if (row.user) {
            return (
                <span key={`btnInProgress${row.id}`}>
                    <button key={`btnInProgress${row.id}`} id={`btnInProgress${row.id}`} className='btn btn-icon-active'>
                        <VisibilityIcon />
                    </button>
                    <UncontrolledTooltip placement='auto' target={`btnInProgress${row.id}`}>
                        อยู่ระหว่างดำเนินการโดย {row.user.fullname}
                    </UncontrolledTooltip>
                </span>
            )
        } else {
            return (
                <span key={`btnInProgress${row.id}`}>
                    <button key={`btnInProgress${row.id}`} id={`btnInProgress${row.id}`} className='btn btn-icon' onClick={e => this.onClickInProgressButton(row.id)}>
                        <VisibilityOffIcon />
                    </button>
                    <UncontrolledTooltip placement='auto' target={`btnInProgress${row.id}`}>
                        ยังไม่ดำเนินการ
                    </UncontrolledTooltip>
                </span>
            )
        }
    }

    getTableBody = () => {
        const {
            transactionBanks,
            selectedSwitches
        } = this.state
        const filterTransactionBanks = _.filter(transactionBanks, obj => obj.customer !== null)
        if (filterTransactionBanks.length === 0) {
            return getNoDataTableBody(6)
        }

        return filterTransactionBanks.map(row => {
            const isChecked = selectedSwitches.indexOf(row.id) !== -1
            return (
                <TableRow key={row.id}>
                    <TableCell>{row.customer && row.customer.fullname}</TableCell>
                    {this.getTableCellBank(row)}
                    <TableCell>{getDateFormat(row.transaction_at)}</TableCell>
                    <TableCell>{thousandFormat(row.amount)}</TableCell>
                    <TableCell>{getTransactionState(row.state)}</TableCell>
                    <TableCell className='text-right'>
                        <FormControlLabel control={
                            <Switch value={row.id} color='primary' onChange={e => this.toggleApprovedSwitch(row)} checked={isChecked} />
                        } label='อนุมัติ' className='mb-0' />
                        <button id={`btnRejected${row.id}`} className='btn btn-icon' onClick={e => this.onClickRejectedButton(row)}>
                            <CancelIcon />
                        </button>
                        <UncontrolledTooltip placement='auto' target={`btnRejected${row.id}`}>
                            ยกเลิกรายการ
                        </UncontrolledTooltip>
                        <button id={`btnView${row.id}`} className='btn btn-icon' onClick={e => this.fetchTransactionBankDetail(row.id)}>
                            <PageviewIcon />
                        </button>
                        <UncontrolledTooltip placement='auto' target={`btnView${row.id}`}>
                            รายละเอียดการทำธุรกรรม
                        </UncontrolledTooltip>
                        {this.getInProgressIcon(row)}
                    </TableCell>
                </TableRow>
            )
        })
    }

    getDepositWaitingTableBody = () => {
        const data = _.filter(this.state.transactionBanks, obj => obj.customer === null)
        if (data.length === 0) return getNoDataTableBody(4)

        return data.map(row => {
            return (
                <TableRow key={row.id}>
                    {this.getTableCellBank(row)}
                    <TableCell>{getDateFormat(row.transaction_at)}</TableCell>
                    <TableCell>{thousandFormat(row.amount)}</TableCell>
                    <TableCell className='text-right'>
                        <button id={`btnDelete${row.id}`} className='btn btn-icon' onClick={e => this.onClickDeleteButton(row)}>
                            <DeleteIcon />
                        </button>
                        <UncontrolledTooltip placement='auto' target={`btnDelete${row.id}`}>
                            ลบข้อมูล
                        </UncontrolledTooltip>
                    </TableCell>
                </TableRow>
            )
        })
    }

    render () {
        const {
            isModalOpen,
            transactionBank
        } = this.state
        return (
            <div className='page-wrapper'>
                <div className='page-breadcrumb'>
                    <h1 className='page-title'>การเงิน</h1>
                    <ol className='breadcrumb'>
                        <li className='breadcrumb-item'>
                            <Link to='/'>หน้าแรก</Link>
                        </li>
                        <li className='breadcrumb-item active'>การเงิน</li>
                    </ol>
                </div>
                <div className='container-fluid'>
                    <Paper className='p-4'>
                        <h4 className='card-title'>รายการธุรกรรม</h4>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell>ชื่อผู้ใช้</TableCell>
                                    <TableCell>บัญชีธนาคาร</TableCell>
                                    <TableCell>วันที่/เวลาทำรายการ</TableCell>
                                    <TableCell>จำนวนเงิน</TableCell>
                                    <TableCell>สถานะ</TableCell>
                                    <TableCell />
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {this.getTableBody()}
                            </TableBody>
                        </Table>
                    </Paper>
                    <Paper className='mt-4 p-4'>
                        <h4 className='card-title'>ยอดรอฝาก</h4>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell>บัญชีธนาคาร</TableCell>
                                    <TableCell>วันที่/เวลาทำรายการ</TableCell>
                                    <TableCell>จำนวนเงิน</TableCell>
                                    <TableCell />
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {this.getDepositWaitingTableBody()}
                            </TableBody>
                        </Table>
                    </Paper>
                </div>
                <ModalTransactionBankDetail transactionBank={transactionBank} isModalOpen={isModalOpen} toggle={this.toggleModal} />
            </div>
        )
    }
}

export default withRouter(BankListView)
