import React, { Component } from 'react'
import { Field, reduxForm, SubmissionError } from 'redux-form'
import { Link, withRouter } from 'react-router-dom'
import _ from 'lodash'
import Swal from 'sweetalert2'
import client from '../../utils/client'
import {
    API_CUSTOMER_BANK_ACCOUNT_URL,
    API_CUSTOMER_CREDIT_ACCOUNT_DETAIL_URL,
    API_CUSTOMER_CREDIT_ACCOUNT_URL,
    API_TRANSACTION_WITHDRAW_URL
} from '../../utils/urls'
import { Button, Card, CardBody } from 'reactstrap'
import Autosuggest from 'react-autosuggest'
import InputField from '../../components/forms/form-input'
import SelectField from '../../components/forms/form-select'
import LayoutTransactionCustomerDetail from '../../components/layouts/layout-transaction-customer-detail'
import { getBankIcon, isCustomerFound } from '../../utils/helpers'

class TransactionRequestWithdrawView extends Component {
    constructor (props) {
        super(props)
        this.state = {
            filterValue: '',
            suggestions: [],
            customerBankAccounts: [],
            customerCreditAccount: null,
            customerCreditAccounts: []
        }
    }

    componentDidMount () {
        this.fetchCustomerCreditAccounts()
    }

    fetchCustomerCreditAccounts = () => {
        client.get(API_CUSTOMER_CREDIT_ACCOUNT_URL, { params: { status: 'approved' } }).then(res => {
            this.setState({ customerCreditAccounts: res.data })
        })
    }

    fetchCustomerCreditAccountDetail = (customerCreditAccountId) => {
        client.get(API_CUSTOMER_CREDIT_ACCOUNT_DETAIL_URL.replace('customerCreditAccountId', customerCreditAccountId)).then(res => {
            this.setState({
                customerCreditAccount: res.data
            })
        })
    }

    fetchCustomerBankAccounts = (customerId) => {
        client.get(API_CUSTOMER_BANK_ACCOUNT_URL, { params: { customer_id: customerId } }).then(res => {
            this.setState({ customerBankAccounts: res.data })
        })
    }

    onChange = (event, { newValue }) => {
        this.setState({
            filterValue: newValue
        })
    }

    onKeyDown = (event) => {
        if (event.keyCode === 13) {
            event.preventDefault()
            this.onSearch()
        }
    }

    onSearch = () => {
        const {
            customerCreditAccounts,
            filterValue
        } = this.state
        const searchValue = filterValue.trim().toLowerCase()
        if (searchValue.length === 0) {
            this.setState({ customerCreditAccount: null })
            return
        }

        const results = customerCreditAccounts.filter(item => isCustomerFound(item, searchValue))
        if (results.length > 0) {
            const suggestion = results[0]
            this.fetchCustomerCreditAccountDetail(suggestion.id)
            this.fetchCustomerBankAccounts(suggestion.customer.id)
        }
    }

    onSuggestionSelected = (event, { suggestion }) => {
        this.fetchCustomerCreditAccountDetail(suggestion.id)
        this.fetchCustomerBankAccounts(suggestion.customer.id)
    }

    onSuggestionsFetchRequested = ({ value }) => {
        const {
            customerCreditAccounts
        } = this.state
        const searchValue = value.trim().toLowerCase()
        const result = customerCreditAccounts.filter(item => isCustomerFound(item, searchValue))
        this.setState({ suggestions: result })
    }

    onSuggestionsClearRequested = () => {
        this.setState({ suggestions: [] })
    };

    getSuggestionValue = (suggestion) => {
        return `[${suggestion.game.name}] ${suggestion.username}`
    }

    renderSuggestion = suggestion => {
        return (
            <span>{`[${suggestion.game.name}] ${suggestion.username}`}</span>
        )
    }

    clearSearch = () => {
        this.setState({
            filterValue: '',
            suggestions: [],
            customerCreditAccount: null
        })
    }

    resetForm = () => {
        this.props.reset()
        this.clearSearch()
    }

    handleSubmitTransaction = (value) => {
        const {
            customerCreditAccount
        } = this.state

        let fieldErrors = {}
        if (!value.amount) {
            fieldErrors.amount = 'จำเป็นต้องกรอก'
        } else if (isNaN(value.amount) && parseInt(value.amount) < 0) {
            fieldErrors.amount = 'จำนวนเงินไม่ถูกต้อง'
        }
        if (!customerCreditAccount) fieldErrors._error = 'จำเป็นต้องเลือกบัญชีของลูกค้า'
        if (!value.customer_bank_account) fieldErrors.system_account = 'จำเป็นต้องกรอก'
        if (_.size(fieldErrors) > 0) throw new SubmissionError(fieldErrors)

        const data = {
            amount: value.amount,
            customer_id: customerCreditAccount.customer.id,
            customer_credit_account_id: customerCreditAccount.id,
            customer_bank_account_id: value.customer_bank_account.id
        }

        client.post(API_TRANSACTION_WITHDRAW_URL, data).then(res => {
            Swal.fire({
                type: 'success',
                title: 'บันทึกข้อมูลสำเร็จ',
                showConfirmButton: false,
                timer: 1500
            })
            this.resetForm()
        })
    }

    render () {
        const {
            filterValue,
            suggestions,
            customerBankAccounts,
            customerCreditAccount
        } = this.state

        const {
            error,
            pristine,
            submitting,
            handleSubmit,
            disabledField
        } = this.props

        const inputProps = {
            placeholder: 'ค้นหาข้อมูลลูกค้า',
            value: filterValue,
            onChange: this.onChange,
            onKeyDown: this.onKeyDown
        }
        return (
            <div className='page-wrapper'>
                <div className='page-breadcrumb'>
                    <h1 className='page-title'>ธุรกรรมใหม่</h1>
                    <ol className='breadcrumb'>
                        <li className='breadcrumb-item'>
                            <Link to='/'>หน้าแรก</Link>
                        </li>
                        <li className='breadcrumb-item active'>ธุรกรรมใหม่</li>
                    </ol>
                </div>
                <div className='container-fluid'>
                    <div className='row'>
                        <div className='col-md-6 col-lg-5'>
                            <Card>
                                <CardBody>
                                    <form onSubmit={handleSubmit(this.handleSubmitTransaction)} >
                                        <div className='form-group'>
                                            <label className='control-label'>ค้นหาลูกค้า</label>
                                            <div className='search-container'>
                                                <Autosuggest
                                                    inputProps={inputProps}
                                                    suggestions={suggestions}
                                                    onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
                                                    onSuggestionsClearRequested={this.onSuggestionsClearRequested}
                                                    onSuggestionSelected={this.onSuggestionSelected}
                                                    getSuggestionValue={this.getSuggestionValue}
                                                    renderSuggestion={this.renderSuggestion}
                                                    shouldRenderSuggestions={() => true} />
                                                {customerCreditAccount && <Button onClick={this.clearSearch} close />}
                                            </div>
                                            {error && <div className='text-danger'>{error}</div>}
                                        </div>
                                        <Field component={SelectField} label='ธนาคาร' name='customer_bank_account' options={customerBankAccounts} getOptionLabel={option => <div>{getBankIcon(option.bank.code)} {option.bank.name} - {option.bank_no}</div>} getOptionValue={option => option.id} isDisabled={disabledField} />
                                        <Field component={InputField} label='จำนวนเงิน' type='number' name='amount' min={0} disabled={disabledField} />
                                        <div className='text-right'>
                                            <Button type='submit' color='info' className='mr-1' disabled={pristine || submitting}>บันทึก</Button>
                                        </div>
                                    </form>
                                </CardBody>
                            </Card>
                        </div>
                        {customerCreditAccount && <div className='col-md-6 col-lg-7'>
                            <LayoutTransactionCustomerDetail customerCreditAccount={customerCreditAccount} />
                        </div>}
                    </div>
                </div>
            </div>
        )
    }
}

const reduxFormRequestWithdrawView = reduxForm({
    form: 'transactionRequestWithdraw',
    enableReinitialize: true
})(TransactionRequestWithdrawView)

export default withRouter(reduxFormRequestWithdrawView)
