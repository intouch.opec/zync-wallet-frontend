import React, { Component } from 'react'
import { Link, withRouter } from 'react-router-dom'
import { API_TRANSACTION_DETAIL_URL } from '../../utils/urls'
import client from '../../utils/client'
import {
    getTransactionStatus,
    getBankIcon,
    getDateFormat,
    getTransactionState,
    thousandFormat
} from '../../utils/helpers'
import { STATE_DEPOSIT, STATE_WITHDRAW } from '../../utils'
import Paper from '@material-ui/core/Paper'

class TransactionDetail extends Component {
    constructor (props) {
        super(props)
        this.state = {
            transaction: {}
        }
    }

    componentDidMount () {
        this.fetchTransaction()
    }

    fetchTransaction = () => {
        const that = this
        const { match: { params } } = that.props
        client.get(API_TRANSACTION_DETAIL_URL.replace('transactionId', params.id)).then(res => {
            that.setState({ transaction: res.data })
        })
    }

    renderTransaction = () => {
        const {
            transaction
        } = this.state
        return (
            <div className='col'>
                <Paper className='p-4 '>
                    <div className='d-flex align-items-center'>
                        <h3 className='card-title'>รายละเอียดธุรกรรม</h3>
                        <h3 className='ml-auto'>{getTransactionState(transaction.state)}</h3>
                    </div>

                    <h5 className='font-weight-bold'>ชื่อลูกค้า</h5>
                    <h6>{transaction.customer && transaction.customer.fullname}</h6>

                    <h5 className='font-weight-bold'>จำนวนเงิน</h5>
                    <h6>{thousandFormat(transaction.amount)}</h6>

                    <h5 className='font-weight-bold'>สถานะโดยรวม</h5>
                    <h6>{getTransactionStatus(transaction.status)}</h6>

                    <h5 className='font-weight-bold'>หมายเหตุ</h5>
                    <h6>{transaction.note}</h6>

                    <h5 className='font-weight-bold'>Remark</h5>
                    <h6>{transaction.remark || '-'}</h6>

                    <h5 className='font-weight-bold'>ทำรายการโดย</h5>
                    <h6>{transaction.user && transaction.user.fullname}</h6>
                </Paper>
            </div>
        )
    }

    renderTransactionBank = () => {
        const {
            transaction
        } = this.state
        const transactionBank = transaction.transaction_bank
        if (!transactionBank) return ''

        let bankAccountNo
        if (transaction.state === STATE_DEPOSIT) {
            bankAccountNo = <h6>{transactionBank.bank_account.name} - {transactionBank.bank_account.account_no}</h6>
        } else {
            bankAccountNo = <h6>{transactionBank.bank_account.bank_no}</h6>
        }

        return (
            <div className='col'>
                <Paper className='p-4 '>
                    <h3 className='card-title'>รายละเอียดการเงิน</h3>

                    <h5 className='font-weight-bold'>เลขที่บัญชี</h5>
                    <h6>{bankAccountNo}</h6>

                    <h5 className='font-weight-bold'>ธนาคาร</h5>
                    <h6>{getBankIcon(transactionBank.bank_account.bank.code)} {transactionBank.bank_account.bank.name}</h6>

                    <h5 className='font-weight-bold'>จำนวนเงิน</h5>
                    <h6>{thousandFormat(transactionBank.amount)}</h6>

                    <h5 className='font-weight-bold'>สถานะ</h5>
                    <h6>{getTransactionStatus(transactionBank.status)}</h6>

                    <h5 className='font-weight-bold'>วันที่ทำรายการ</h5>
                    <h6>{getDateFormat(transactionBank.transaction_at)}</h6>

                    <h5 className='font-weight-bold'>ทำรายการโดย</h5>
                    <h6>{transactionBank.user && transactionBank.user.fullname}</h6>
                </Paper>
            </div>
        )
    }

    renderTransactionCredit = () => {
        const {
            transaction
        } = this.state
        const transactionCredit = transaction.transaction_credit
        if (!transactionCredit) return ''

        return (
            <div className='col'>
                <Paper className='p-4 '>
                    <h3 className='card-title'>รายละเอียดเครดิต</h3>

                    <h5 className='font-weight-bold'>Username</h5>
                    <h6>{transactionCredit.customer}</h6>

                    <h5 className='font-weight-bold'>เครดิต</h5>
                    <h6>{thousandFormat(transactionCredit.credit)}</h6>

                    <h5 className='font-weight-bold'>โบนัสเครดิต</h5>
                    <h6>{thousandFormat(transactionCredit.bonus_credit)}</h6>

                    <h5 className='font-weight-bold'>รวม</h5>
                    <h6>{thousandFormat(transactionCredit.total_credit)}</h6>

                    <h5 className='font-weight-bold'>สถานะ</h5>
                    <h6>{getTransactionStatus(transactionCredit.status)}</h6>

                    <h5 className='font-weight-bold'>วันที่ทำรายการ</h5>
                    <h6>{getDateFormat(transactionCredit.updated_at)}</h6>

                    <h5 className='font-weight-bold'>ทำรายการโดย</h5>
                    <h6>{transactionCredit.user && transactionCredit.user.fullname}</h6>
                </Paper>
            </div>
        )
    }

    renderHistoryContent = () => {
        const {
            transaction
        } = this.state

        if (transaction.state === STATE_DEPOSIT) {
            return (
                <div className='row'>
                    {this.renderTransaction()}
                    {this.renderTransactionBank()}
                    {this.renderTransactionCredit()}
                </div>
            )
        } else {
            return (
                <div className='row'>
                    {this.renderTransaction()}
                    {this.renderTransactionCredit()}
                    {this.renderTransactionBank()}
                </div>
            )
        }
    }

    render () {
        return (
            <div className='page-wrapper'>
                <div className='page-breadcrumb'>
                    <h3 className='page-title'>รายละเอียดธุรกรรม</h3>
                    <ol className='breadcrumb'>
                        <li className='breadcrumb-item'>
                            <Link to='/'>หน้าแรก</Link>
                        </li>
                        <li className='breadcrumb-item'>
                            <Link to='/transactions/'>รายการธุรกรรม</Link>
                        </li>
                        <li className='breadcrumb-item active'>รายละเอียดธุรกรรม</li>
                    </ol>
                </div>
                <div className='container-fluid'>
                    {this.renderHistoryContent()}
                </div>
            </div>
        )
    }
}

export default withRouter(TransactionDetail)
