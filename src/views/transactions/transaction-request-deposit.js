import React, { Component } from 'react'
import { Field, reduxForm, SubmissionError } from 'redux-form'
import { Link, withRouter } from 'react-router-dom'
import _ from 'lodash'
import Swal from 'sweetalert2'
import client from '../../utils/client'
import {
    API_SYSTEM_BANK_ACCOUNT_URL,
    API_CUSTOMER_CREDIT_ACCOUNT_DETAIL_URL,
    API_CUSTOMER_CREDIT_ACCOUNT_URL, API_TRANSACTION_DEPOSIT_URL
} from '../../utils/urls'
import { Button, Card, CardBody } from 'reactstrap'
import Autosuggest from 'react-autosuggest'
import InputField from '../../components/forms/form-input'
import SelectField from '../../components/forms/form-select'
import TextareaField from '../../components/forms/form-textarea'
import LayoutTransactionCustomerDetail from '../../components/layouts/layout-transaction-customer-detail'
import InputFileField from '../../components/forms/form-input-file'
import PusherService from '../../utils/pusher'
import uuidV4 from 'uuid/v4'
import { getBankIcon, isCustomerFound } from '../../utils/helpers'
import InputDatePickerField from '../../components/forms/form-date-picker'
import moment from 'moment'

class TransactionRequestDepositView extends Component {
    constructor (props) {
        super(props)
        this.state = {
            filterValue: '',
            suggestions: [],
            systemBankAccounts: [],
            customerCreditAccount: null,
            customerCreditAccounts: [],
            slipUploadKey: uuidV4(),
            date: new Date(),
            time: new Date()
        }
    }

    componentDidMount () {
        this.fetchSystemBankAccounts()
        this.fetchCustomerCreditAccounts()

        const channelCustomer = PusherService.getInstance().subscribe('customer')
        channelCustomer.bind('new', (data) => {
            this.setState({
                customerCreditAccounts: [ ...this.state.customerCreditAccounts, data ]
            })
        })
    }

    fetchSystemBankAccounts = () => {
        client.get(API_SYSTEM_BANK_ACCOUNT_URL, { params: { is_active: true } }).then(res => {
            this.setState({ systemBankAccounts: res.data })
        })
    }

    fetchCustomerCreditAccounts = () => {
        client.get(API_CUSTOMER_CREDIT_ACCOUNT_URL, { params: { status: 'approved' } }).then(res => {
            this.setState({ customerCreditAccounts: res.data })
        })
    }

    fetchCustomerCreditAccountDetail = (customerCreditAccountId) => {
        client.get(API_CUSTOMER_CREDIT_ACCOUNT_DETAIL_URL.replace('customerCreditAccountId', customerCreditAccountId)).then(res => {
            this.setState({
                customerCreditAccount: res.data
            })
        })
    }

    onChange = (event, { newValue }) => {
        this.setState({
            filterValue: newValue
        })
    }

    onKeyDown = (event) => {
        if (event.keyCode === 13) {
            event.preventDefault()
            this.onSearch()
        }
    }

    onSearch = () => {
        const {
            customerCreditAccounts,
            filterValue
        } = this.state
        const searchValue = filterValue.trim().toLowerCase()
        if (searchValue.length === 0) {
            this.setState({ customerCreditAccount: null })
            return
        }

        const results = customerCreditAccounts.filter(item => isCustomerFound(item, searchValue))
        if (results.length > 0) this.fetchCustomerCreditAccountDetail(results[0].id)
    }

    onSuggestionSelected = (event, { suggestion }) => {
        this.fetchCustomerCreditAccountDetail(suggestion.id)
    }

    onSuggestionsFetchRequested = ({ value }) => {
        const {
            customerCreditAccounts
        } = this.state
        const searchValue = value.trim().toLowerCase()
        const result = customerCreditAccounts.filter(item => isCustomerFound(item, searchValue))
        this.setState({ suggestions: result })
    }

    onSuggestionsClearRequested = () => {
        this.setState({ suggestions: [] })
    };

    getSuggestionValue = (suggestion) => {
        return `[${suggestion.game.name}] ${suggestion.username}`
    }

    renderSuggestion = suggestion => {
        return (
            <span>{`[${suggestion.game.name}] ${suggestion.username}`}</span>
        )
    }

    clearSearch = () => {
        this.setState({
            filterValue: '',
            suggestions: [],
            customerCreditAccount: null
        })
    }

    resetForm = () => {
        this.props.reset()
        this.clearSearch()
        this.clearFileInput()
    }

    clearFileInput = () => {
        this.props.change('slip_upload', null)
        this.setState({ slipUploadKey: uuidV4() })
    }

    handleSubmitTransaction = (value) => {
        const {
            date,
            time,
            customerCreditAccount
        } = this.state

        const transactionDate = moment(date).format('YYYY-MM-DD')
        const transactionTime = moment(time).format('HH:mm:ss')
        let transactionDateTime = moment(`${transactionDate}T${transactionTime}+07:00`)
        if (transactionDateTime.isValid()) {
            transactionDateTime = transactionDateTime.utc().format('YYYY-MM-DD HH:mm:ss')
        } else {
            transactionDateTime = null
        }

        let fieldErrors = {}
        // eslint-disable-next-line no-undef
        let formData = new FormData()
        if (!value.amount) {
            fieldErrors.amount = 'จำเป็นต้องกรอก'
        } else if (isNaN(value.amount) && parseInt(value.amount) < 0) {
            fieldErrors.amount = 'จำนวนเงินไม่ถูกต้อง'
        }
        if (!value.system_bank_account) fieldErrors.system_bank_account = 'จำเป็นต้องกรอก'
        if (_.size(fieldErrors) > 0) throw new SubmissionError(fieldErrors)

        if (customerCreditAccount) {
            formData.append('customer_id', customerCreditAccount.customer.id)
            formData.append('customer_credit_account_id', customerCreditAccount.id)
        }

        formData.append('system_bank_account_id', value.system_bank_account.id)
        formData.append('amount', value.amount)
        formData.append('transaction_at', transactionDateTime)

        if (value.note) {
            formData.append('note', value.note)
        }
        if (value.slip_upload) {
            formData.append('slip_upload', value.slip_upload)
        }

        client.post(API_TRANSACTION_DEPOSIT_URL, formData, { headers: { 'content-type': 'multipart/form-data' } }).then(res => {
            Swal.fire({
                type: 'success',
                title: 'บันทึกข้อมูลสำเร็จ',
                showConfirmButton: false,
                timer: 1500
            })
            this.resetForm()
        })
    }

    onDatePickerChange = (input, currentDate, dateFormat) => {
        input.onChange(moment(currentDate).format(dateFormat))
        this.setState({ [input.name]: currentDate })
    }

    render () {
        const {
            filterValue,
            suggestions,
            systemBankAccounts,
            customerCreditAccount,
            slipUploadKey
        } = this.state

        const {
            error,
            pristine,
            submitting,
            handleSubmit
        } = this.props

        const inputProps = {
            placeholder: 'ค้นหาข้อมูลลูกค้า',
            value: filterValue,
            onChange: this.onChange,
            onKeyDown: this.onKeyDown
        }
        return (
            <div className='page-wrapper'>
                <div className='page-breadcrumb'>
                    <h1 className='page-title'>ธุรกรรมใหม่</h1>
                    <ol className='breadcrumb'>
                        <li className='breadcrumb-item'>
                            <Link to='/'>หน้าแรก</Link>
                        </li>
                        <li className='breadcrumb-item active'>ธุรกรรมใหม่</li>
                    </ol>
                </div>
                <div className='container-fluid'>
                    <div className='row'>
                        <div className='col-md-6 col-lg-5'>
                            <Card>
                                <CardBody>
                                    <form onSubmit={handleSubmit(this.handleSubmitTransaction)} >
                                        <div className='form-group'>
                                            <label className='control-label'>ค้นหาลูกค้า</label>
                                            <div className='search-container'>
                                                <Autosuggest
                                                    inputProps={inputProps}
                                                    suggestions={suggestions}
                                                    onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
                                                    onSuggestionsClearRequested={this.onSuggestionsClearRequested}
                                                    onSuggestionSelected={this.onSuggestionSelected}
                                                    getSuggestionValue={this.getSuggestionValue}
                                                    renderSuggestion={this.renderSuggestion}
                                                    shouldRenderSuggestions={() => true} />
                                                {customerCreditAccount && <Button onClick={this.clearSearch} close />}
                                            </div>
                                        </div>
                                        <Field component={SelectField} label='ธนาคาร' name='system_bank_account' options={systemBankAccounts} getOptionLabel={option => <div>{getBankIcon(option.bank.code)} {option.bank.name} - {option.name} ({option.account_no})</div>} getOptionValue={option => option.id} />
                                        <Field component={InputField} label='จำนวนเงิน' type='number' name='amount' min={0} />
                                        <div className='row'>
                                            <div className='col-md-7'>
                                                <Field label='วันที่' name='date' dateFormat='YYYY-MM-DD'
                                                    maxDate={new Date()} component={InputDatePickerField}
                                                    selected={this.state.date}
                                                    onDatePickerChange={this.onDatePickerChange} />
                                            </div>
                                            <div className='col-md-5'>
                                                <Field label='เวลา' name='time' dateFormat='HH:mm'
                                                    timeIntervals={15} component={InputDatePickerField}
                                                    selected={this.state.time}
                                                    onDatePickerChange={this.onDatePickerChange}
                                                    showTimeInput showTimeSelect showTimeSelectOnly />
                                            </div>
                                        </div>
                                        <Field component={TextareaField} label='หมายเหตุ' name='note' rows={4} />
                                        <Field key={slipUploadKey} component={InputFileField} label='รูปสลิป' name='slip_upload' accept='image/*' clearFileInput={this.clearFileInput} />
                                        <div className='text-right'>
                                            <Button type='submit' color='info' className='mr-1' disabled={pristine || submitting}>บันทึก</Button>
                                        </div>
                                    </form>
                                </CardBody>
                            </Card>
                        </div>
                        {customerCreditAccount && <div className='col-md-6 col-lg-7'>
                            <LayoutTransactionCustomerDetail customerCreditAccount={customerCreditAccount} />
                        </div>}
                    </div>
                </div>
            </div>
        )
    }
}

const reduxFormRequestDepositView = reduxForm({
    form: 'transactionRequestDeposit',
    enableReinitialize: true
})(TransactionRequestDepositView)

export default withRouter(reduxFormRequestDepositView)
