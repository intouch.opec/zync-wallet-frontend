import React, { Component } from 'react'
import { Link, withRouter } from 'react-router-dom'
import { API_USER_DETAIL_URL } from '../../utils/urls'
import client from '../../utils/client'
import FormUser from '../../components/form-layouts/form-layout-user'
import Swal from 'sweetalert2'
import ModalChangePassword from '../../components/modals/modal-change-password'
import { Button } from 'reactstrap'

class UserEditView extends Component {
    constructor (props) {
        super(props)
        this.state = {
            user: null,
            isModalOpen: false
        }
    }

    componentDidMount () {
        this.fetchUser()
    }

    fetchUser = () => {
        const {
            match: { params }
        } = this.props
        client.get(API_USER_DETAIL_URL.replace('userId', params.id)).then(res => {
            this.setState({ user: res.data })
        })
    }

    toggleModal = () => {
        this.setState({ isModalOpen: !this.state.isModalOpen })
    }

    handleSubmitUser = (value, reset) => {
        const {
            match: { params }
        } = this.props
        client.put(API_USER_DETAIL_URL.replace('userId', params.id), value).then(res => {
            this.setState({
                user: value
            }, () => {
                Swal.fire({
                    type: 'success',
                    title: 'บันทึกข้อมูลสำเร็จ',
                    showConfirmButton: false,
                    timer: 1500
                })
            })
        })
    }

    render () {
        const {
            match: { params }
        } = this.props

        const {
            user,
            isModalOpen
        } = this.state
        return (
            <div className='page-wrapper'>
                <div className='page-breadcrumb'>
                    <h1 className='page-title'>รายละเอียดพนักงาน</h1>
                    <nav aria-label='breadcrumb'>
                        <ol className='breadcrumb'>
                            <li className='breadcrumb-item'>
                                <Link to='/'>หน้าแรก</Link>
                            </li>
                            <li className='breadcrumb-item'>
                                <Link to='/users'>พนักงาน</Link>
                            </li>
                            <li className='breadcrumb-item active'>รายละเอียดพนักงาน</li>
                        </ol>
                    </nav>
                </div>
                <div className='container-fluid'>
                    <div className='card m-4'>
                        <div className='card-body'>
                            <div className='row'>
                                <div className='col-md-6'>
                                    <h4 className='card-title'>แก้ไขพนักงาน {user && user.username}</h4>
                                </div>
                                <div className='col-md-6 text-right'>
                                    <Button color='link' className='p-0' onClick={this.toggleModal}>เปลี่ยนรหัสผ่าน</Button>
                                </div>
                            </div>
                            <FormUser initialValues={user} handleSubmitUser={this.handleSubmitUser} editUser />
                            <ModalChangePassword userId={params.id} isModalOpen={isModalOpen} toggle={this.toggleModal} />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default withRouter(UserEditView)
