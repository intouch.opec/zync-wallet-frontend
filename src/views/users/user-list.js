import React, { Component } from 'react'
import { Link, withRouter } from 'react-router-dom'
import { API_USER_URL, API_USER_DETAIL_URL } from '../../utils/urls'
import client from '../../utils/client'
import Edit from '@material-ui/icons/Edit'
import AddIcon from '@material-ui/icons/Add'
import Switch from '@material-ui/core/Switch'
import CancelIcon from '@material-ui/icons/Cancel'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import Table from '@material-ui/core/Table'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import TableCell from '@material-ui/core/TableCell'
import TableBody from '@material-ui/core/TableBody'
import Paper from '@material-ui/core/Paper'
import { getNoDataTableBody } from '../../components/table'
import { UncontrolledTooltip } from 'reactstrap'
import { getYesNoStatus } from '../../utils/helpers'
import _ from 'lodash'
import Swal from 'sweetalert2'

class UserListView extends Component {
    constructor (props) {
        super(props)
        this.state = {
            users: []
        }
    }

    componentDidMount () {
        this.fetchCustomers()
    }

    fetchCustomers = () => {
        client.get(API_USER_URL).then(res => {
            this.setState({ users: res.data })
        })
    }

    toggleIsActiveSwitch = (row) => {
        const isActive = !row.is_active
        client.put(API_USER_DETAIL_URL.replace('userId', row.id), { is_active: isActive }).then(res => {
            const users = this.state.users.map(obj => {
                if (obj.id === row.id) {
                    obj.is_active = isActive
                }
                return obj
            })
            this.setState({ users })
        })
    }

    removeUser = (row) => {
        Swal.fire({
            title: 'ยืนยันการลบข้อมูล',
            type: 'question',
            showCancelButton: true,
            showCloseButton: true
        }).then(result => {
            if (result.value) {
                client.delete(API_USER_DETAIL_URL.replace('userId', row.id)).then(res => {
                    const users = _.filter(this.state.users, obj => obj.id !== row.id)
                    this.setState({ users }, () => {
                        Swal.fire({
                            type: 'success',
                            title: 'ลบข้อมูลสำเร็จ',
                            showConfirmButton: false,
                            timer: 1500
                        })
                    })
                })
            }
        })
    }

    renderTableBody = () => {
        const {
            users
        } = this.state
        if (users.length === 0) {
            return getNoDataTableBody(5)
        }

        return users.map(row => {
            return (
                <TableRow key={row.id}>
                    <TableCell>{row.username}</TableCell>
                    <TableCell>{row.fullname}</TableCell>
                    <TableCell>{row.group_name || '-'}</TableCell>
                    <TableCell>{getYesNoStatus(row.is_staff)}</TableCell>
                    <TableCell className='text-right'>
                        <FormControlLabel control={
                            <Switch color='primary' value={row.id} checked={row.is_active} onChange={e => this.toggleIsActiveSwitch(row)} />
                        } label='ใช้งาน' className='mb-0' />
                        <Link to={`/users/${row.id}`} >
                            <button id={`btnEdit${row.id}`} className='btn btn-icon'><Edit /></button>
                        </Link>
                        <UncontrolledTooltip placement='auto' target={`btnEdit${row.id}`}>แก้ไขข้อมูล</UncontrolledTooltip>
                        <button id={`btnRejected${row.id}`} className='btn btn-icon' onClick={e => this.removeUser(row)}><CancelIcon /></button>
                        <UncontrolledTooltip placement='auto' target={`btnRejected${row.id}`}>ลบข้อมูล</UncontrolledTooltip>
                    </TableCell>
                </TableRow>
            )
        })
    }

    render () {
        return (
            <div className='page-wrapper'>
                <div className='page-breadcrumb'>
                    <h1 className='page-title'>พนักงาน</h1>
                    <ol className='breadcrumb'>
                        <li className='breadcrumb-item'>
                            <Link to='/'>หน้าแรก</Link>
                        </li>
                        <li className='breadcrumb-item active'>พนักงาน</li>
                    </ol>
                </div>
                <div className='container-fluid'>
                    <Paper className='p-4'>
                        <div className='d-flex align-items-center'>
                            <h4 className='card-title'>รายชื่อพนักงาน</h4>
                            <div className='ml-auto'>
                                <Link to='/users/create' className='btn btn-info' >
                                    <AddIcon /> เพิ่มพนักงาน
                                </Link>
                            </div>
                        </div>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell>Username</TableCell>
                                    <TableCell>ชื่อ</TableCell>
                                    <TableCell>กลุ่ม</TableCell>
                                    <TableCell>สถานะแอดมิน</TableCell>
                                    <TableCell />
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {this.renderTableBody()}
                            </TableBody>
                        </Table>
                    </Paper>
                </div>
            </div>)
    }
}
export default withRouter(UserListView)
