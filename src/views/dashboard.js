import React from 'react'
import { Link, withRouter } from 'react-router-dom'
import { API_DASHBOARD_URL } from '../utils/urls'
import client from '../utils/client'
import { Paper, Table, TableHead, TableRow, TableCell, TableBody } from '@material-ui/core'
import { getNoDataTableBody } from '../components/table'
import PageviewIcon from '@material-ui/icons/Pageview'
import { UncontrolledTooltip } from 'reactstrap'
import { getTransactionState, getTransactionStatus, getDateFormat, getBankIcon, thousandFormat } from '../utils/helpers'

const cardSummary = [
    { stateIndex: 'TransactionDepositAmount', name: 'ยอดฝาก', labelColor: 'text-success' },
    { stateIndex: 'TransactionWithdrawAmount', name: 'ยอดถอน', labelColor: 'text-danger' },
    { stateIndex: 'TransactionDepositWaitingAmount', name: 'ยอดรอการฝาก', labelColor: 'text-warning' },
    { stateIndex: 'customerTotal', name: 'ลูกค้า', labelColor: 'text-info' }
]

class DashboardView extends React.Component {
    constructor (props) {
        super(props)
        this.state = {
            transactions: [],
            systemBankAccounts: [],
            systemCreditAccounts: [],
            customerTotal: 0,
            TransactionDepositAmount: 0,
            TransactionWithdrawAmount: 0,
            TransactionDepositWaitingAmount: 0
        }
    }

    componentDidMount () {
        this.fetchDashboard()
    }

    fetchDashboard = () => {
        client.get(API_DASHBOARD_URL).then(res => {
            this.setState({
                transactions: res.data.transactions,
                systemBankAccounts: res.data.system_bank_accounts,
                systemCreditAccounts: res.data.system_credit_accounts,
                customerTotal: res.data.customer_total || 0,
                TransactionDepositAmount: res.data.transaction_deposit_amount || 0,
                TransactionWithdrawAmount: res.data.transaction_withdraw_amount || 0,
                TransactionDepositWaitingAmount: res.data.transaction_deposit_waiting_amount || 0
            })
        })
    }

    renderCard = () => {
        return (
            cardSummary.map(({ stateIndex, name, labelColor }, index) => (
                <div key={index} className='col-lg-3 col-md-6'>
                    <Paper className='p-4 mt-1'>
                        <h4>{name}</h4>
                        <h3 className={labelColor}>{thousandFormat(this.state[stateIndex])}</h3>
                    </Paper>
                </div>)
            )
        )
    }

    getSystemBankAccountTableBody = () => {
        const {
            systemBankAccounts
        } = this.state
        if (systemBankAccounts.length === 0) {
            return getNoDataTableBody(7)
        }
        return systemBankAccounts.map(row => {
            return (
                <TableRow key={row.id}>
                    <TableCell>{row.name}</TableCell>
                    <TableCell>{row.account_no}</TableCell>
                    <TableCell className='text-nowrap'>{getBankIcon(row.bank.code)} {row.bank.name}</TableCell>
                    <TableCell>{thousandFormat(row.balance)}</TableCell>
                    <TableCell className='text-warning'>{thousandFormat(row.waiting_balance)}</TableCell>
                    <TableCell className='text-info'>{thousandFormat(row.total_balance)}</TableCell>
                </TableRow>
            )
        })
    }

    getSystemCreditAccountTableBody = () => {
        const {
            systemCreditAccounts
        } = this.state
        if (systemCreditAccounts.length === 0) {
            return getNoDataTableBody(5)
        }
        return systemCreditAccounts.map(row => {
            return (
                <TableRow key={row.id}>
                    <TableCell>{row.name}</TableCell>
                    <TableCell>{row.game.name}</TableCell>
                    <TableCell>{thousandFormat(row.balance)}</TableCell>
                </TableRow>
            )
        })
    }

    getTransactionHistoryTableBody () {
        const {
            transactions
        } = this.state
        if (transactions.length === 0) {
            return getNoDataTableBody(7)
        }
        return transactions.map(row => (
            <TableRow key={row.id}>
                <TableCell>{row.customer && row.customer.fullname}</TableCell>
                <TableCell>{getTransactionState(row.state)}</TableCell>
                <TableCell>{getDateFormat(row.created_at)}</TableCell>
                <TableCell>{row.customer_credit_account && row.customer_credit_account.game.name}</TableCell>
                <TableCell>{thousandFormat(row.amount)}</TableCell>
                <TableCell>{getTransactionStatus(row.status)}</TableCell>
                <TableCell>{row.user && row.user.fullname}</TableCell>
                <TableCell className='text-right'>
                    <Link id={`btnView${row.id}`} to={`/transactions/${row.id}`} className='btn btn-icon'>
                        <PageviewIcon />
                    </Link>
                    <UncontrolledTooltip placement='auto' target={`btnView${row.id}`}>
                        ดูข้อมูล
                    </UncontrolledTooltip>
                </TableCell>
            </TableRow>
        ))
    }

    render () {
        return (
            <div className='page-wrapper'>
                <div className='page-breadcrumb'>
                    <h4 className='page-title'>แดชบอร์ด</h4>
                    <nav aria-label='breadcrumb'>
                        <ol className='breadcrumb'>
                            <li className='breadcrumb-item'>
                                <Link to='/'>หน้าแรก</Link>
                            </li>
                            <li className='breadcrumb-item active'>แดชบอร์ด</li>
                        </ol>
                    </nav>
                </div>

                <div className='container-fluid'>
                    <div className='row mb-3'>
                        {this.renderCard()}
                    </div>
                    <div className='row mb-3'>
                        <div className='col-md-4'>
                            <Paper className='p-4'>
                                <h4>บัญชีเครดิต</h4>
                                <Table>
                                    <TableHead>
                                        <TableRow>
                                            <TableCell>ชื่อบัญชี</TableCell>
                                            <TableCell>เกม</TableCell>
                                            <TableCell>จำนวนเงิน</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        {this.getSystemCreditAccountTableBody()}
                                    </TableBody>
                                </Table>
                            </Paper>
                        </div>
                        <div className='col-md-8'>
                            <Paper className='p-4'>
                                <h4>บัญชีธนาคาร</h4>
                                <Table>
                                    <TableHead>
                                        <TableRow>
                                            <TableCell>ชื่อบัญชี</TableCell>
                                            <TableCell>หมายเลขบัญชี</TableCell>
                                            <TableCell>ธนาคาร</TableCell>
                                            <TableCell>จำนวนเงิน</TableCell>
                                            <TableCell>จำนวนเงินรอฝาก</TableCell>
                                            <TableCell>รวม</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        {this.getSystemBankAccountTableBody()}
                                    </TableBody>
                                </Table>
                            </Paper>
                        </div>
                    </div>
                    <Paper className='mb-3 p-4'>
                        <h4>ประวัติการทำธุรกรรม</h4>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell>ลูกค้า</TableCell>
                                    <TableCell>ประเภทการทำรายการ</TableCell>
                                    <TableCell>วันที่</TableCell>
                                    <TableCell>เกม</TableCell>
                                    <TableCell>จำนวนเงิน</TableCell>
                                    <TableCell>สถานะ</TableCell>
                                    <TableCell>เจ้าหน้าที่</TableCell>
                                    <TableCell />
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {this.getTransactionHistoryTableBody()}
                            </TableBody>
                        </Table>
                    </Paper>
                </div>
            </div>
        )
    }
}

export default withRouter(DashboardView)
