import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link, withRouter } from 'react-router-dom'
import { API_SYSTEM_BANK_ACCOUNT_DETAIL_URL } from '../../../utils/urls'
import client from '../../../utils/client'
import SystemAccountBankForm from '../../../components/form-layouts/form-layout-system-bank-account'
import Swal from 'sweetalert2'

class SystemBankAccountEditView extends Component {
    constructor (props) {
        super(props)
        this.state = {
            systemAccount: {}
        }
    }

    componentDidMount () {
        this.fetchSystemAccount()
    }

    fetchSystemAccount = () => {
        const { match: { params } } = this.props
        client.get(API_SYSTEM_BANK_ACCOUNT_DETAIL_URL.replace('systemBankAccountId', params.id)).then(res => {
            this.setState({ systemAccount: res.data })
        })
    }

    onFormSubmit = (value, reset) => {
        const {
            match: { params }
        } = this.props
        client.put(API_SYSTEM_BANK_ACCOUNT_DETAIL_URL.replace('systemBankAccountId', params.id), value).then(res => {
            this.setState({ systemAccount: res.data })
            Swal.fire({
                type: 'success',
                title: 'บันทึกข้อมูลสำเร็จ',
                showConfirmButton: false,
                timer: 1500
            })
        })
    }

    render () {
        const {
            systemAccount
        } = this.state
        return (
            <div className='page-wrapper'>
                <div className='page-breadcrumb'>
                    <h1 className='page-title'>รายละเอียดบัญชีธนาคาร</h1>
                    <ol className='breadcrumb'>
                        <li className='breadcrumb-item'>
                            <Link to='/'>หน้าแรก</Link>
                        </li>
                        <li className='breadcrumb-item'>
                            <Link to='/system-accounts'>บัญชีระบบ</Link>
                        </li>
                        <li className='breadcrumb-item'>
                            <Link to='/system-accounts/banks'>ธนาคาร</Link>
                        </li>
                        <li className='breadcrumb-item active'>รายละเอียดบัญชีธนาคาร</li>
                    </ol>
                </div>
                <div className='container-fluid'>
                    <div className='card m-4'>
                        <div className='card-body'>
                            <h4 className='card-title'>แก้ไขบัญชีธนาคาร</h4>
                            <SystemAccountBankForm initialValues={systemAccount} onFormSubmit={this.onFormSubmit} usernameRequired />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps (state) {
    return { ...state }
}

export default withRouter(
    connect(mapStateToProps)(SystemBankAccountEditView)
)
