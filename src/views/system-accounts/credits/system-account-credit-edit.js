import React, { Component } from 'react'
import { Link, withRouter } from 'react-router-dom'
import client from '../../../utils/client'
import SystemAccountCreditForm from '../../../components/form-layouts/form-layout-system-credit-account'
import Swal from 'sweetalert2'
import { API_SYSTEM_CREDIT_ACCOUNT_DETAIL_URL } from '../../../utils/urls'

class SystemCreditAccountEditView extends Component {
    constructor (props) {
        super(props)
        this.state = {
            systemAccount: {}
        }
    }

    componentDidMount () {
        this.fetchSystemAccount()
    }

    fetchSystemAccount = () => {
        const { match: { params } } = this.props
        client.get(API_SYSTEM_CREDIT_ACCOUNT_DETAIL_URL.replace('systemCreditAccountId', params.id)).then(res => {
            this.setState({ systemAccount: res.data })
        })
    }

    onFormSubmit = (value, reset) => {
        const {
            match: { params }
        } = this.props
        client.put(API_SYSTEM_CREDIT_ACCOUNT_DETAIL_URL.replace('systemCreditAccountId', params.id), value).then(res => {
            this.setState({ systemAccount: res.data })
            Swal.fire({
                type: 'success',
                title: 'บันทึกข้อมูลสำเร็จ',
                showConfirmButton: false,
                timer: 1500
            })
        })
    }

    render () {
        const {
            systemAccount
        } = this.state
        return (
            <div className='page-wrapper'>
                <div className='page-breadcrumb'>
                    <h1 className='page-title'>รายละเอียดบัญชีเครดิค</h1>
                    <ol className='breadcrumb'>
                        <li className='breadcrumb-item'>
                            <Link to='/'>หน้าแรก</Link>
                        </li>
                        <li className='breadcrumb-item'>
                            <Link to='/system-accounts'>บัญชีระบบ</Link>
                        </li>
                        <li className='breadcrumb-item'>
                            <Link to='/system-accounts/credits'>เครดิต</Link>
                        </li>
                        <li className='breadcrumb-item active'>รายละเอียดบัญชีเครดิต</li>
                    </ol>
                </div>
                <div className='container-fluid'>
                    <div className='card m-4'>
                        <div className='card-body'>
                            <h4 className='card-title'>แก้ไขบัญชีเครดิต</h4>
                            <SystemAccountCreditForm initialValues={systemAccount} onFormSubmit={this.onFormSubmit} />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default withRouter(SystemCreditAccountEditView)
