import React, { Component } from 'react'
import { Link, withRouter } from 'react-router-dom'
import client from '../../utils/client'
import EditIcon from '@material-ui/icons/Edit'
import AddIcon from '@material-ui/icons/Add'
import Swal from 'sweetalert2'
import DeleteIcon from '@material-ui/icons/Delete'
import Table from '@material-ui/core/Table/index'
import TableHead from '@material-ui/core/TableHead/index'
import TableRow from '@material-ui/core/TableRow/index'
import TableCell from '@material-ui/core/TableCell/index'
import TableBody from '@material-ui/core/TableBody/index'
import Paper from '@material-ui/core/Paper/index'
import { getNoDataTableBody } from '../../components/table'
import { Button, UncontrolledTooltip } from 'reactstrap'
import _ from 'lodash'
import ModalSettingCustomerLevel from '../../components/modals/modal-setting-customer-level'
import { API_CUSTOMER_LEVEL_DETAIL_URL, API_CUSTOMER_LEVEL_URL } from '../../utils/urls'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import Switch from '@material-ui/core/Switch'

class SettingCustomerLevel extends Component {
    constructor (props) {
        super(props)
        this.state = {
            isModalOpen: false,
            customerLevels: [],
            selectedCustomerLevel: null
        }
    }

    componentDidMount () {
        this.fetchCustomerLevels()
    }

    fetchCustomerLevels = () => {
        client.get(API_CUSTOMER_LEVEL_URL).then(res => {
            this.setState({ customerLevels: res.data })
        })
    }

    toggleModal = () => {
        this.setState({
            isModalOpen: !this.state.isModalOpen,
            selectedGame: null,
            apiEndpoint: API_CUSTOMER_LEVEL_URL
        })
    }

    onCustomerLevelSubmitCallback = (res) => {
        const {
            customerLevels
        } = this.state
        const game = res.data
        let newGames
        if (customerLevels.filter(item => item.id === game.id).length > 0) {
            newGames = customerLevels.map(item => item.id === game.id ? { ...game } : item)
        } else {
            newGames = [...customerLevels, game]
        }
        this.setState({
            isModalOpen: false,
            customerLevels: newGames,
            selectedGame: null,
            apiEndpoint: API_CUSTOMER_LEVEL_URL
        })
    }

    toggleIsActiveSwitch = (row) => {
        const isActive = !row.is_active
        client.put(API_CUSTOMER_LEVEL_DETAIL_URL.replace('customerLevelId', row.id), { is_active: isActive }).then(res => {
            const customerLevels = this.state.customerLevels.map(obj => {
                if (obj.id === row.id) obj.is_active = isActive
                return obj
            })
            this.setState({ customerLevels })
        })
    }

    editCustomerLevel = (row) => {
        this.setState({
            isModalOpen: true,
            selectedGame: row,
            apiEndpoint: API_CUSTOMER_LEVEL_DETAIL_URL.replace('customerLevelId', row.id)
        })
    }

    removeCustomerLevel = (row) => {
        Swal.fire({
            title: 'ยืนยันการลบข้อมูล',
            type: 'question',
            showCancelButton: true,
            showCloseButton: true
        }).then(result => {
            if (result.value) {
                client.delete(API_CUSTOMER_LEVEL_DETAIL_URL.replace('customerLevelId', row.id)).then(res => {
                    const customerLevels = _.filter(this.state.customerLevels, obj => obj.id !== row.id)
                    this.setState({ customerLevels }, () => {
                        Swal.fire({
                            type: 'success',
                            title: 'ลบข้อมูลสำเร็จ',
                            showConfirmButton: false,
                            timer: 1500
                        })
                    })
                })
            }
        })
    }

    getTableBody = () => {
        const {
            customerLevels
        } = this.state
        if (customerLevels.length === 0) {
            return getNoDataTableBody(2)
        }
        return customerLevels.map(row => {
            let actionTabelCell
            if (row.is_default) {
                actionTabelCell = (
                    <TableCell className='text-right'>
                        <button id={`btnEdit${row.id}`} className='btn btn-icon' onClick={e => this.editCustomerLevel(row)}>
                            <EditIcon />
                        </button>
                        <UncontrolledTooltip placement='auto' target={`btnEdit${row.id}`}>แก้ไขข้อมูล</UncontrolledTooltip>
                    </TableCell>
                )
            } else {
                actionTabelCell = (
                    <TableCell className='text-right'>
                        <FormControlLabel control={
                            <Switch color='primary' value={row.id} checked={row.is_active} onChange={e => this.toggleIsActiveSwitch(row)} />
                        } label='ใช้งาน' className='mb-0' />
                        <button id={`btnEdit${row.id}`} className='btn btn-icon' onClick={e => this.editCustomerLevel(row)}>
                            <EditIcon />
                        </button>
                        <UncontrolledTooltip placement='auto' target={`btnEdit${row.id}`}>แก้ไขข้อมูล</UncontrolledTooltip>
                        <button id={`btnRejected${row.id}`} className='btn btn-icon' onClick={e => this.removeCustomerLevel(row)}>
                            <DeleteIcon />
                        </button>
                        <UncontrolledTooltip placement='auto' target={`btnRejected${row.id}`}>ลบข้อมูล</UncontrolledTooltip>
                    </TableCell>
                )
            }
            return (
                <TableRow key={row.id}>
                    <TableCell>{row.name}</TableCell>
                    {actionTabelCell}
                </TableRow>
            )
        })
    }

    render () {
        const {
            isModalOpen,
            selectedGame,
            apiEndpoint
        } = this.state
        return (
            <div className='page-wrapper'>
                <div className='page-breadcrumb'>
                    <h1 className='page-title'>ระดับลูกค้า</h1>
                    <ol className='breadcrumb'>
                        <li className='breadcrumb-item'>
                            <Link to='/'>หน้าแรก</Link>
                        </li>
                        <li className='breadcrumb-item active'>ระดับลูกค้า</li>
                    </ol>
                </div>

                <div className='container-fluid'>
                    <Paper className='p-4'>
                        <div className='d-flex align-items-center'>
                            <h4 className='card-title'>รายชื่อระดับลูกค้า</h4>
                            <div className='ml-auto'>
                                <Button color='info' onClick={this.toggleModal}>
                                    <AddIcon /> เพิ่มระดับลูกค้า
                                </Button>
                            </div>
                        </div>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell>ชื่อระดับลูกค้า</TableCell>
                                    <TableCell />
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {this.getTableBody()}
                            </TableBody>
                        </Table>
                    </Paper>
                </div>
                <ModalSettingCustomerLevel apiEndpoint={apiEndpoint} initialValues={selectedGame} isModalOpen={isModalOpen} toggle={this.toggleModal} onSubmitCallback={this.onCustomerLevelSubmitCallback} />
            </div>
        )
    }
}

export default withRouter(SettingCustomerLevel)
