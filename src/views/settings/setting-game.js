import React, { Component } from 'react'
import { Link, withRouter } from 'react-router-dom'
import { API_GAME_DETAIL_URL, API_GAME_URL } from '../../utils/urls'
import client from '../../utils/client'
import EditIcon from '@material-ui/icons/Edit'
import AddIcon from '@material-ui/icons/Add'
import Swal from 'sweetalert2'
import DeleteIcon from '@material-ui/icons/Delete'
import Table from '@material-ui/core/Table/index'
import TableHead from '@material-ui/core/TableHead/index'
import TableRow from '@material-ui/core/TableRow/index'
import TableCell from '@material-ui/core/TableCell/index'
import TableBody from '@material-ui/core/TableBody/index'
import Paper from '@material-ui/core/Paper/index'
import { getNoDataTableBody } from '../../components/table'
import { Button, UncontrolledTooltip } from 'reactstrap'
import _ from 'lodash'
import ModalSettingGame from '../../components/modals/modal-setting-game'
import { thousandFormat } from '../../utils/helpers'

class SettingGameView extends Component {
    constructor (props) {
        super(props)
        this.state = {
            isModalOpen: false,
            games: [],
            selectedGame: null,
            apiEndpoint: API_GAME_URL
        }
    }

    componentDidMount () {
        this.fetchGames()
    }

    fetchGames = () => {
        client.get(API_GAME_URL).then(res => {
            this.setState({ games: res.data })
        })
    }

    toggleModal = () => {
        this.setState({
            isModalOpen: !this.state.isModalOpen,
            selectedGame: null,
            apiEndpoint: API_GAME_URL
        })
    }

    onGameSubmitCallback = (res) => {
        const {
            games
        } = this.state
        const game = res.data
        let newGames
        if (games.filter(item => item.id === game.id).length > 0) {
            newGames = games.map(item => item.id === game.id ? { ...game } : item)
        } else {
            newGames = [...games, game]
        }
        this.setState({
            isModalOpen: false,
            games: newGames,
            selectedGame: null,
            apiEndpoint: API_GAME_URL
        })
    }

    editGame = (row) => {
        this.setState({
            isModalOpen: true,
            selectedGame: row,
            apiEndpoint: API_GAME_DETAIL_URL.replace('gameId', row.id)
        })
    }

    removeGame = (row) => {
        Swal.fire({
            title: 'ยืนยันการลบข้อมูล',
            type: 'question',
            showCancelButton: true,
            showCloseButton: true
        }).then(result => {
            if (result.value) {
                client.delete(API_GAME_DETAIL_URL.replace('gameId', row.id)).then(res => {
                    const games = _.filter(this.state.games, obj => obj.id !== row.id)
                    this.setState({ games }, () => {
                        Swal.fire({
                            type: 'success',
                            title: 'ลบข้อมูลสำเร็จ',
                            showConfirmButton: false,
                            timer: 1500
                        })
                    })
                })
            }
        })
    }

    getTableBody = () => {
        const {
            games
        } = this.state
        if (games.length === 0) {
            return getNoDataTableBody(3)
        }
        return games.map(row => {
            return (
                <TableRow key={row.id}>
                    <TableCell>{row.name}</TableCell>
                    <TableCell>{thousandFormat(row.convert)}</TableCell>
                    <TableCell className='text-right'>
                        <button id={`btnEdit${row.id}`} className='btn btn-icon' onClick={e => this.editGame(row)}>
                            <EditIcon />
                        </button>
                        <UncontrolledTooltip placement='auto' target={`btnEdit${row.id}`}>แก้ไขข้อมูล</UncontrolledTooltip>
                        <button id={`btnRejected${row.id}`} className='btn btn-icon' onClick={e => this.removeGame(row)}>
                            <DeleteIcon />
                        </button>
                        <UncontrolledTooltip placement='auto' target={`btnRejected${row.id}`}>ลบข้อมูล</UncontrolledTooltip>
                    </TableCell>
                </TableRow>
            )
        })
    }

    render () {
        const {
            isModalOpen,
            selectedGame,
            apiEndpoint
        } = this.state
        return (
            <div className='page-wrapper'>
                <div className='page-breadcrumb'>
                    <h1 className='page-title'>เกม</h1>
                    <ol className='breadcrumb'>
                        <li className='breadcrumb-item'>
                            <Link to='/'>หน้าแรก</Link>
                        </li>
                        <li className='breadcrumb-item active'>เกม</li>
                    </ol>
                </div>

                <div className='container-fluid'>
                    <Paper className='p-4'>
                        <div className='d-flex align-items-center'>
                            <h4 className='card-title'>รายชื่อเกม</h4>
                            <div className='ml-auto'>
                                <Button color='info' onClick={this.toggleModal}>
                                    <AddIcon /> เพิ่มเกม
                                </Button>
                            </div>
                        </div>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell>ชื่อเกม</TableCell>
                                    <TableCell>อัตราแลกเปลี่ยน (บาท/เครดิต)</TableCell>
                                    <TableCell />
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {this.getTableBody()}
                            </TableBody>
                        </Table>
                    </Paper>
                </div>
                <ModalSettingGame apiEndpoint={apiEndpoint} initialValues={selectedGame} isModalOpen={isModalOpen} toggle={this.toggleModal} onSubmitCallback={this.onGameSubmitCallback} />
            </div>
        )
    }
}

export default withRouter(SettingGameView)
