import {
    AUTH_AUTHENTICATED,
    AUTH_UNAUTHENTICATED
} from '../utils'

const initState = {
    url: '',
    email: '',
    username: '',
    first_name: '',
    last_name: '',
    groups: [],
    error: null
}

export default function authReducer (state = initState, action) {
    switch (action.type) {
    case AUTH_AUTHENTICATED:
        window.localStorage.setItem('accessToken', action.payload.access)
        window.localStorage.setItem('refreshToken', action.payload.refresh)
        delete action.payload.access
        delete action.payload.refresh
        return {
            ...state,
            ...action.payload
        }
    case AUTH_UNAUTHENTICATED:
        window.localStorage.removeItem('accessToken')
        window.localStorage.removeItem('refreshToken')
        return initState
    }
    return state
}
