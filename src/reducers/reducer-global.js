import {
    GLOBAL_ADD_LOADER, GLOBAL_REMOVE_LOADER
} from '../utils/index'

const initState = {
    loading: []
}

export default function globalReducer (state = initState, action) {
    switch (action.type) {
    case GLOBAL_ADD_LOADER:
        return {
            ...state,
            loading: [ ...state.loading, action.payload ]
        }
    case GLOBAL_REMOVE_LOADER:
        return {
            ...state,
            loading: state.loading.filter(item => item !== action.payload)
        }
    }
    return state
}
