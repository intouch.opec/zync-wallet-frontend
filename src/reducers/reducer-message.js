import {
    AUTH_AUTHENTICATION_ERROR
} from '../utils'

const initState = {
    authMessage: null
}

export default function messageReducer (state = initState, action) {
    switch (action.type) {
    case AUTH_AUTHENTICATION_ERROR:
        return {
            ...state,
            authMessage: {
                messageType: 'error',
                message: action.payload
            }
        }
    }
    return state
}
