import { GLOBAL_ADD_LOADER, GLOBAL_REMOVE_LOADER } from '../utils'

export function addLoader (requestId) {
    return (dispatch) => {
        dispatch({
            type: GLOBAL_ADD_LOADER,
            payload: requestId
        })
    }
}

export function removeLoader (requestId) {
    return (dispatch) => {
        dispatch({
            type: GLOBAL_REMOVE_LOADER,
            payload: requestId
        })
    }
}
