import React from 'react'
import { createBrowserHistory } from 'history'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import { connect } from 'react-redux'
import Preloader from './components/preloader'
import AuthorizedRoute from './components/mixins/authorized-route'
import UnauthorizedRoute from './components/mixins/unauthorized-route'
import LayoutAuthenticated from './components/layouts/layout-authenticated'
import LoginView from './views/login'
import Error404View from './views/error404'
import PusherService from './utils/pusher'
import { createNotification, requestPermission } from './utils/push-notification'

const hist = createBrowserHistory()

class App extends React.Component {
    componentDidMount () {
        requestPermission()
        let channelAlert = PusherService.getInstance().subscribe('system')
        channelAlert.bind('alert', (data) => {
            createNotification(data.message)
        })
    }

    render () {
        const {
            global: { loading }
        } = this.props
        return (
            <Router history={hist}>
                {loading.length > 0 && <Preloader />}
                <Switch>
                    <UnauthorizedRoute path='/auth' component={LoginView} />
                    <AuthorizedRoute component={LayoutAuthenticated} />
                    <Route component={Error404View} />
                </Switch>
            </Router>
        )
    }
}

function mapStateToProps (state) {
    const { global } = state
    return {
        global
    }
}

export default connect(mapStateToProps)(App)
