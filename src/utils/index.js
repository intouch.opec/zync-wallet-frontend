export const PUSHER_APP_KEY = process.env.MIX_PUSHER_APP_KEY

export const GLOBAL_ADD_LOADER = 'GLOBAL_ADD_LOADER'
export const GLOBAL_REMOVE_LOADER = 'GLOBAL_REMOVE_LOADER'

export const AUTH_AUTHENTICATED = 'AUTH_AUTHENTICATED'
export const AUTH_UNAUTHENTICATED = 'AUTH_UNAUTHENTICATED'
export const AUTH_AUTHENTICATION_ERROR = 'AUTH_AUTHENTICATION_ERROR'

export const STATUS_PENDING = 0
export const STATUS_APPROVED = 1
export const STATUS_REJECTED = 2

export const STATE_DEPOSIT = 1
export const STATE_WITHDRAW = 2

export const SELECT_OPTION_STATE = [{
    label: 'ฝากเงิน',
    value: STATE_DEPOSIT
}, {
    label: 'ถอนเงิน',
    value: STATE_WITHDRAW
}]
