import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import DashboardIcon from '@material-ui/icons/Dashboard'
import AccountBalanceWalletIcon from '@material-ui/icons/AccountBalanceWallet'
import KeyboardArrowRightIcon from '@material-ui/icons/KeyboardArrowRight'
import AccountBalanceIcon from '@material-ui/icons/AccountBalance'
import AccountBoxIcon from '@material-ui/icons/AccountBox'
import PeopleIcon from '@material-ui/icons/People'
import HistoryIcon from '@material-ui/icons/History'
import SettingsIcon from '@material-ui/icons/Settings'
import ShareIcon from '@material-ui/icons/Share'
import VerifiedUserIcon from '@material-ui/icons/VerifiedUser'
import DashboardView from '../views/dashboard'
import TransactionRequestDepositView from '../views/transactions/transaction-request-deposit'
import TransactionRequestWithdrawView from '../views/transactions/transaction-request-withdraw'
import TransactionBankView from '../views/transactions/transaction-bank'
import TransactionCreditView from '../views/transactions/transaction-credit'
import TransactionListView from '../views/transactions/transaction-list'
import TransactionDetail from '../views/transactions/transaction-detail'
import CustomerListView from '../views/customers/customer-list'
import CustomerCreateView from '../views/customers/customer-create'
import CustomerDetailView from '../views/customers/customer-detail'
import CustomerEditView from '../views/customers/customer-edit'
import UserListView from '../views/users/user-list'
import UserCreateView from '../views/users/user-create'
import UserEditView from '../views/users/user-edit'
import SystemBankAccountListView from '../views/system-accounts/banks/system-bank-account-list'
import SystemBankAccountCreateView from '../views/system-accounts/banks/system-bank-account-create'
import SystemBankAccountEditView from '../views/system-accounts/banks/system-bank-account-edit'
import SystemCreditAccountListView from '../views/system-accounts/credits/system-account-credit-list'
import SystemCreditAccountCreateView from '../views/system-accounts/credits/system-account-credit-create'
import SystemCreditAccountEditView from '../views/system-accounts/credits/system-account-credit-edit'
import SettingGameView from '../views/settings/setting-game'
import SettingCustomerLevelView from '../views/settings/setting-customer-level'
import PromotionListView from '../views/promotions/promotion-list'
import PromotionCreateView from '../views/promotions/promotion-create'
import PromotionEditView from '../views/promotions/promotion-edit'
import OpenAccountRequestView from '../views/open-account-requests'

const routes = [{
    path: '/',
    to: '/dashboard',
    isShowOnMenu: false,
    isRedirectRoute: true
}, {
    ordering: 1,
    path: '/dashboard',
    name: 'แดชบอร์ด',
    icon: <DashboardIcon />,
    isShowOnMenu: true,
    component: DashboardView
}, {
    ordering: 5,
    path: '/transactions',
    name: 'ประวัติการทำธุรกรรม',
    icon: <HistoryIcon />,
    isShowOnMenu: true,
    component: TransactionListView,
    requiredPermission: 'apps.can_access_transaction_history'
}, {
    ordering: 2,
    path: '/transactions/request-deposit',
    name: 'ธุรกรรมฝากใหม่',
    icon: <FontAwesomeIcon icon='terminal' />,
    isShowOnMenu: true,
    component: TransactionRequestDepositView
}, {
    ordering: 2,
    path: '/transactions/request-withdraw',
    name: 'ธุรกรรมถอนใหม่',
    icon: <FontAwesomeIcon icon='terminal' />,
    isShowOnMenu: true,
    component: TransactionRequestWithdrawView
}, {
    ordering: 3,
    path: '/transactions/banks',
    name: 'การเงิน',
    icon: <FontAwesomeIcon icon='money-check-alt' />,
    isShowOnMenu: true,
    component: TransactionBankView,
    requiredPermission: 'apps.can_access_transaction_bank'
}, {
    ordering: 4,
    path: '/transactions/credits',
    name: 'เครดิต',
    icon: <AccountBalanceWalletIcon />,
    isShowOnMenu: true,
    component: TransactionCreditView,
    requiredPermission: 'apps.can_access_transaction_credit'
}, {
    path: '/transactions/:id',
    isShowOnMenu: false,
    component: TransactionDetail,
    requiredPermission: 'apps.can_access_transaction_history'
}, {
    ordering: 5,
    path: '/customers/request',
    name: 'คำขอเปิดบัญชี',
    icon: <VerifiedUserIcon />,
    isShowOnMenu: true,
    component: OpenAccountRequestView
}, {
    ordering: 6,
    path: '/customers',
    name: 'ลูกค้า',
    icon: <PeopleIcon />,
    isShowOnMenu: true,
    component: CustomerListView
}, {
    path: '/customers/create',
    isShowOnMenu: false,
    component: CustomerCreateView
}, {
    path: '/customers/:id',
    isShowOnMenu: false,
    component: CustomerDetailView
}, {
    path: '/customers/:id/edit',
    isShowOnMenu: false,
    component: CustomerEditView
}, {
    ordering: 7,
    path: '/users',
    name: 'พนักงาน',
    icon: <AccountBoxIcon />,
    isShowOnMenu: true,
    component: UserListView,
    requiredPermission: 'apps.can_access_user'
}, {
    path: '/users/create',
    isShowOnMenu: false,
    component: UserCreateView,
    requiredPermission: 'apps.can_access_user'
}, {
    path: '/users/:id',
    isShowOnMenu: true,
    component: UserEditView,
    requiredPermission: 'apps.can_access_user'
}, {
    ordering: 8,
    path: '/promotions',
    name: 'โปรโมชั่น',
    icon: <ShareIcon />,
    isShowOnMenu: true,
    component: PromotionListView,
    requiredPermission: 'apps.can_access_promotion'
}, {
    path: '/promotions/create',
    isShowOnMenu: false,
    component: PromotionCreateView,
    requiredPermission: 'apps.can_access_promotion'
}, {
    path: '/promotions/:id',
    isShowOnMenu: false,
    component: PromotionEditView,
    requiredPermission: 'apps.can_access_promotion'
}, {
    id: 'systemAccount',
    ordering: 9,
    name: 'บัญชีระบบ',
    icon: <AccountBalanceIcon />,
    isShowOnMenu: true,
    requiredPermission: 'apps.can_access_system_account',
    subRoutes: [{
        path: '/system-accounts',
        to: '/system-accounts/banks',
        isShowOnMenu: false,
        isRedirectRoute: true
    }, {
        ordering: 1,
        path: '/system-accounts/banks',
        name: 'บัญชีธนาคาร',
        icon: <KeyboardArrowRightIcon />,
        isShowOnMenu: true,
        component: SystemBankAccountListView,
        requiredPermission: 'apps.can_access_system_account'
    }, {
        path: '/system-accounts/banks/create',
        isShowOnMenu: false,
        component: SystemBankAccountCreateView,
        requiredPermission: 'apps.can_access_system_account'
    }, {
        path: '/system-accounts/banks/:id',
        isShowOnMenu: false,
        component: SystemBankAccountEditView,
        requiredPermission: 'apps.can_access_system_account'
    }, {
        ordering: 2,
        path: '/system-accounts/credits',
        name: 'บัญชีเครดิต',
        icon: <KeyboardArrowRightIcon />,
        isShowOnMenu: true,
        component: SystemCreditAccountListView,
        requiredPermission: 'apps.can_access_system_account'
    }, {
        path: '/system-accounts/credits/create',
        isShowOnMenu: false,
        component: SystemCreditAccountCreateView,
        requiredPermission: 'apps.can_access_system_account'
    }, {
        path: '/system-accounts/credits/:id',
        isShowOnMenu: false,
        component: SystemCreditAccountEditView,
        requiredPermission: 'apps.can_access_system_account'
    }]
}, {

    id: 'settings',
    ordering: 10,
    name: 'ตั้งค่า',
    icon: <SettingsIcon />,
    isShowOnMenu: true,
    requiredPermission: 'apps.can_access_setting',
    subRoutes: [{
        ordering: 1,
        path: '/games',
        name: 'เกม',
        icon: <KeyboardArrowRightIcon />,
        isShowOnMenu: true,
        component: SettingGameView,
        requiredPermission: 'apps.can_access_setting'
    }, {
        ordering: 2,
        path: '/customer-levels',
        name: 'ระดับลูกค้า',
        icon: <KeyboardArrowRightIcon />,
        isShowOnMenu: true,
        component: SettingCustomerLevelView,
        requiredPermission: 'apps.can_access_setting'
    }]
}]

export default routes
