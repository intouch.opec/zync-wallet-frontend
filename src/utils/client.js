import axios from 'axios'
import { API_AUTH_REFRESH_TOKEN_URL, API_URL } from './urls'
import { store } from './store'
import { AUTH_UNAUTHENTICATED, GLOBAL_ADD_LOADER, GLOBAL_REMOVE_LOADER } from './index'
import Swal from 'sweetalert2'
import uuidV4 from 'uuid/v4'

let clientRefreshTokenRequest

const getClientRefreshTokenRequest = () => {
    if (!clientRefreshTokenRequest) {
        const refreshToken = window.localStorage.getItem('refreshToken')
        clientRefreshTokenRequest = noRetryClient.post(API_AUTH_REFRESH_TOKEN_URL, { 'refresh': refreshToken })
        clientRefreshTokenRequest.then(resetClientRefreshTokenRequest)
    }
    return clientRefreshTokenRequest
}

const resetClientRefreshTokenRequest = () => {
    clientRefreshTokenRequest = null
}

const doErrorResponse = (err) => {
    if (err.response.status === 401) {
        store.dispatch({ type: AUTH_UNAUTHENTICATED })
    } else if (err.response.status >= 500) {
        Swal.fire({
            type: 'error',
            title: 'มีบางอย่างผิดพลาดกรุณาติดต่อผู้ดูแลระบบ',
            showConfirmButton: false,
            timer: 1500
        })
    }
}

const baseConfig = {
    baseURL: API_URL
}

const baseRequest = (config) => {
    const uuid = uuidV4()
    const accessToken = window.localStorage.getItem('accessToken')
    config.headers.Authorization = accessToken ? `Bearer ${accessToken}` : ''
    config.requestLoaderId = uuid
    store.dispatch({
        type: GLOBAL_ADD_LOADER,
        payload: uuid
    })
    return config
}

const baseResponse = (response) => {
    store.dispatch({
        type: GLOBAL_REMOVE_LOADER,
        payload: response.config.requestLoaderId
    })
    return response
}

let noRetryClient = axios.create(baseConfig)
noRetryClient.interceptors.request.use(baseRequest)

noRetryClient.interceptors.response.use(baseResponse, error => {
    store.dispatch({
        type: GLOBAL_REMOVE_LOADER,
        payload: error.response.config.requestLoaderId
    })
    doErrorResponse(error)
    return Promise.reject(error)
})

let client = axios.create(baseConfig)
client.interceptors.request.use(baseRequest)
client.interceptors.response.use(baseResponse, error => {
    store.dispatch({
        type: GLOBAL_REMOVE_LOADER,
        payload: error.response.config.requestLoaderId
    })
    if (error.response.status === 401) {
        return getClientRefreshTokenRequest()
            .then(response => {
                window.localStorage.setItem('accessToken', response.data.access)
                error.response.config.headers.Authorization = `Bearer ${response.data.access}`
                return noRetryClient(error.response.config)
            })
    } else {
        doErrorResponse(error)
    }
    return Promise.reject(error)
})

export default client
