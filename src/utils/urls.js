export const API_URL = process.env.MIX_API_URL || 'http://localhost:8000'

export const API_AUTH_URL = `/api/v1/auth/`
export const API_AUTH_REFRESH_TOKEN_URL = `/api/v1/auth/refresh/`

export const API_TRANSACTION_URL = `/api/v1/transactions/`
export const API_TRANSACTION_DEPOSIT_URL = `/api/v1/transactions/deposit/`
export const API_TRANSACTION_WITHDRAW_URL = `/api/v1/transactions/withdraw/`
export const API_TRANSACTION_DETAIL_URL = `/api/v1/transactions/transactionId/`
export const API_TRANSACTION_BANK_URL = `/api/v1/transactions/banks/`
export const API_TRANSACTION_BANK_DETAIL_URL = `/api/v1/transactions/banks/transactionBankId/`
export const API_TRANSACTION_CREDIT_URL = `/api/v1/transactions/credits/`
export const API_TRANSACTION_CREDIT_DETAIL_URL = `/api/v1/transactions/credits/transactionCreditId/`

export const API_CUSTOMER_URL = `/api/v1/customers/`
export const API_CUSTOMER_DETAIL_URL = `/api/v1/customers/customerId/`
export const API_CUSTOMER_LEVEL_URL = `/api/v1/customerlevels/`
export const API_CUSTOMER_LEVEL_DETAIL_URL = `/api/v1/customerlevels/customerLevelId/`
export const API_CUSTOMER_BANK_ACCOUNT_URL = `/api/v1/customerbankaccounts/`
export const API_CUSTOMER_BANK_ACCOUNT_DETAIL_URL = `/api/v1/customerbankaccounts/customerBankAccountId/`
export const API_CUSTOMER_CREDIT_ACCOUNT_URL = `/api/v1/customercreditaccounts/`
export const API_CUSTOMER_CREDIT_ACCOUNT_DETAIL_URL = `/api/v1/customercreditaccounts/customerCreditAccountId/`

export const API_USER_URL = `/api/v1/users/`
export const API_USER_DETAIL_URL = `/api/v1/users/userId/`
export const API_USER_CHANGE_PASSWORD_URL = `/api/v1/users/userId/changepassword/`

export const API_GAME_URL = `/api/v1/games/`
export const API_GAME_DETAIL_URL = `/api/v1/games/gameId/`

export const API_BANK_URL = `/api/v1/banks/`

export const API_GROUP_URL = `/api/v1/groups/`
export const API_PERMISSION_URL = `/api/v1/permissions/`

export const API_PROMOTION_URL = `/api/v1/promotions/`
export const API_PROMOTION_DETAIL_URL = `/api/v1/promotions/promotionId/`

export const API_SYSTEM_BANK_ACCOUNT_URL = `/api/v1/systembankaccounts/`
export const API_SYSTEM_BANK_ACCOUNT_DETAIL_URL = `/api/v1/systembankaccounts/systemBankAccountId/`

export const API_SYSTEM_CREDIT_ACCOUNT_URL = `/api/v1/systemcreditaccounts/`
export const API_SYSTEM_CREDIT_ACCOUNT_DETAIL_URL = `/api/v1/systemcreditaccounts/systemCreditAccountId/`

export const API_DASHBOARD_URL = `/api/v1/dashboard/`
