import Pusher from 'pusher-js'
import { PUSHER_APP_KEY } from './index'

export default (() => {
    let instance

    const createInstance = () => {
        const config = {
            cluster: 'ap1',
            forceTLS: true
        }
        return new Pusher(PUSHER_APP_KEY, config)
    }
    return {
        getInstance: () => {
            if (!instance) {
                instance = createInstance()
            }
            return instance
        }
    }
})()
