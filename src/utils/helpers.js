import React from 'react'
import moment from 'moment'
import numeral from 'numeral'
import { STATE_DEPOSIT, STATE_WITHDRAW, STATUS_APPROVED, STATUS_PENDING, STATUS_REJECTED } from './index'
import BayBankIcon from '../../assets/images/bank-logos/bay.svg'
import BblBankIcon from '../../assets/images/bank-logos/bbl.svg'
import KbankBankIcon from '../../assets/images/bank-logos/kbank.svg'
import KtbBankIcon from '../../assets/images/bank-logos/ktb.svg'
import ScbBankIcon from '../../assets/images/bank-logos/scb.svg'
import TbankBankIcon from '../../assets/images/bank-logos/tbank.svg'
import TmbBankIcon from '../../assets/images/bank-logos/tmb.svg'
import BaacBankIcon from '../../assets/images/bank-logos/baac.svg'
import CimbBankIcon from '../../assets/images/bank-logos/cimb.svg'
import CitiBankIcon from '../../assets/images/bank-logos/citi.svg'
import GhbBankIcon from '../../assets/images/bank-logos/ghb.svg'
import GsbBankIcon from '../../assets/images/bank-logos/gsb.svg'
import IbankBankIcon from '../../assets/images/bank-logos/ibank.svg'
import IcbcBankIcon from '../../assets/images/bank-logos/icbc.svg'
import KkBankIcon from '../../assets/images/bank-logos/kk.svg'
import LhbBankIcon from '../../assets/images/bank-logos/lhb.svg'
import TiscoBankIcon from '../../assets/images/bank-logos/tisco.svg'
import UobBankIcon from '../../assets/images/bank-logos/uob.svg'

export const isCustomerFound = (item, keyword) => {
    const {
        game,
        username,
        customer
    } = item
    return username.toLowerCase().includes(keyword) ||
        game.name.toLowerCase().includes(keyword) ||
        customer.email.toLowerCase().includes(keyword) ||
        customer.fullname.toLowerCase().includes(keyword) ||
        customer.line_id.toLowerCase().includes(keyword) ||
        customer.telephone.toLowerCase().includes(keyword)
}

export const getYesNoStatus = (status) => {
    let statusElement = null
    if (status) {
        statusElement = <span className='text-success'>Yes</span>
    } else {
        statusElement = <span className='text-danger'>No</span>
    }
    return statusElement
}

export const getTransactionState = (transactionState) => {
    let stateElement = null
    if (transactionState === STATE_DEPOSIT) {
        stateElement = <span className='text-success'>ฝาก</span>
    } else if (transactionState === STATE_WITHDRAW) {
        stateElement = <span className='text-danger'>ถอน</span>
    }
    return stateElement
}

export const getTransactionStatus = (transactionStatus) => {
    let statusElement = null
    if (transactionStatus === STATUS_PENDING) {
        statusElement = <span className='text-info'>อยู่ระหว่างดำเนินการ</span>
    } else if (transactionStatus === STATUS_APPROVED) {
        statusElement = <span className='text-success'>อนุมัติ</span>
    } else if (transactionStatus === STATUS_REJECTED) {
        statusElement = <span className='text-danger'>ยกเลิกรายการ</span>
    }
    return statusElement
}

export const getDateFormat = (date, formatString) => {
    let transactionTime = '-'
    if (date) {
        transactionTime = moment(date).format(formatString || 'DD/MM/YYYY HH:mm')
    }
    return transactionTime
}

export const thousandFormat = (number) => {
    return numeral(number).format('0,0[.]00')
}

export const isPermissive = (auth, requiredPermission) => {
    let permissive = true
    if (requiredPermission) {
        permissive = auth.is_superuser || auth.is_staff || (auth.permissions || []).includes(requiredPermission)
    }
    return permissive
}

export const getBankIcon = (bankCode) => {
    switch (bankCode) {
    case 'baac':
        return <BaacBankIcon color='#4b9b1d' width={14} height={14} className='mr-2' />
    case 'bay':
        return <BayBankIcon color='#fec43b' width={14} height={14} className='mr-2' />
    case 'bbl':
        return <BblBankIcon color='#1e4598' width={14} height={14} className='mr-2' />
    case 'cimb':
        return <CimbBankIcon color='#7e2f36' width={14} height={14} className='mr-2' />
    case 'citi':
        return <CitiBankIcon color='#1583c7' width={14} height={14} className='mr-2' />
    case 'ghb':
        return <GhbBankIcon color='#f57d23' width={14} height={14} className='mr-2' />
    case 'gsb':
        return <GsbBankIcon color='#eb198d' width={14} height={14} className='mr-2' />
    case 'ibank':
        return <IbankBankIcon color='#184615' width={14} height={14} className='mr-2' />
    case 'icbc':
        return <IcbcBankIcon color='#c50f1c' width={14} height={14} className='mr-2' />
    case 'kbank':
        return <KbankBankIcon color='#138f2d' width={14} height={14} className='mr-2' />
    case 'kk':
        return <KkBankIcon color='#199cc5' width={14} height={14} className='mr-2' />
    case 'ktb':
        return <KtbBankIcon color='#1ba5e1' width={14} height={14} className='mr-2' />
    case 'lhb':
        return <LhbBankIcon color='#6d6e71' width={14} height={14} className='mr-2' />
    case 'scb':
        return <ScbBankIcon color='#4e2e7f' width={14} height={14} className='mr-2' />
    case 'tbank':
        return <TbankBankIcon color='#fc4f1f' width={14} height={14} className='mr-2' />
    case 'tisco':
        return <TiscoBankIcon color='#12549f' width={14} height={14} className='mr-2' />
    case 'tmb':
        return <TmbBankIcon color='#1279be' width={14} height={14} className='mr-2' />
    case 'uob':
        return <UobBankIcon color='#0b3979' width={14} height={14} className='mr-2' />
    default:
        return ''
    }
}
