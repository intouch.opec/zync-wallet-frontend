import React, { Component } from 'react'
import CKEditor from '@ckeditor/ckeditor5-react'
import ClassicEditor from '@ckeditor/ckeditor5-build-classic'
import { FormFeedback, FormGroup, Label } from 'reactstrap'

class CKEditorField extends Component {
    render () {
        const {
            input,
            label,
            meta: {
                touched,
                error
            }
        } = this.props
        const isError = (touched && error)
        return (
            <FormGroup >
                {label && <Label for={`${input.name}-${label}`}>{label}</Label>}
                <CKEditor
                    {...input}
                    {...this.props}
                    id={`${input.name}-${label}`}
                    editor={ClassicEditor}
                    data={input.value}
                    onChange={(event, editor) => input.onChange(editor.getData())}
                    onBlur={(editor) => input.onBlur(input.value)} />
                {isError && <FormFeedback>{error}</FormFeedback>}
            </FormGroup>
        )
    }
}

export default CKEditorField
