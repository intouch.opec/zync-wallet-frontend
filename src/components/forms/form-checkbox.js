import React, { Component } from 'react'
import { FormFeedback, FormGroup, Input, Label } from 'reactstrap'

class CheckboxField extends Component {
    render () {
        const {
            input,
            label,
            meta: {
                touched,
                error
            }
        } = this.props
        const isError = (touched && error)
        return (
            <FormGroup check>
                <Label check>
                    <Input {...input} {...this.props} type='checkbox' invalid={isError} checked={input.value} />
                    {label}
                </Label>
                {isError && <FormFeedback>{error}</FormFeedback>}
            </FormGroup>
        )
    }
}

export default CheckboxField
