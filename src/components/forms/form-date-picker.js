import React, { Component } from 'react'
import DatePicker from 'react-datepicker'
import { FormFeedback, FormGroup, Label } from 'reactstrap'

class InputDatePickerField extends Component {
    componentDidMount () {
        this.onDatePickerChange(this.props.selected || new Date())
    }

    onDatePickerChange = (currentDate) => {
        const {
            input,
            dateFormat,
            onDatePickerChange
        } = this.props
        onDatePickerChange(input, currentDate, dateFormat)
    }

    render () {
        const {
            input,
            label,
            meta: {
                touched,
                error
            }
        } = this.props
        return (
            <FormGroup>
                {label && <Label for={`${input.name}-${label}`}>{label}</Label>}
                <DatePicker
                    {...input}
                    {...this.props}
                    id={`${input.name}-${label}`}
                    className='form-control'
                    onChange={this.onDatePickerChange} />
                {touched && error && <FormFeedback invalid>{error}</FormFeedback>}
            </FormGroup>
        )
    }
}

export default InputDatePickerField
