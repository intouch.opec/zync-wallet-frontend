import React from 'react'
import { Link, withRouter } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import LogoIcon from '../../assets/images/logo-light-icon.png'
import LogoText from '../../assets/images/logo-light-text.png'
import LogoZyncIcon from '../../assets/images/logo-zync-icon.png'

import CloseIcon from '@material-ui/icons/Close'
import MenuIcon from '@material-ui/icons/Menu'
import MoreHorizIcon from '@material-ui/icons/MoreHoriz'

class TopBarHeader extends React.Component {
    constructor (props) {
        super(props)
        this.state = {
            isShowTopHeaderProfile: false,
            isShowProfileDropdown: false
        }
    }

    onClickShowSideBar = () => {
        this.props.onClickShowSideBar()
    }

    onClickShowProfile = () => {
        this.setState({
            isShowTopHeaderProfile: !this.state.isShowTopHeaderProfile,
            isShowProfileDropdown: false
        })
    }

    onClickShowProfileDropdown = () => {
        this.setState({
            isShowProfileDropdown: !this.state.isShowProfileDropdown
        })
    }

    render () {
        const {
            isShowTopHeaderProfile,
            isShowProfileDropdown
        } = this.state

        const {
            isShowSideBar
        } = this.props
        return (
            <header className='topbar' >
                <nav className='navbar top-navbar navbar-expand-md navbar-light'>
                    <div className='navbar-header'>
                        <a href='javascript:' className='nav-toggler d-block d-md-none' onClick={this.onClickShowSideBar}>
                            {isShowSideBar ? <CloseIcon /> : <MenuIcon />}
                        </a>
                        <div className='navbar-brand'>
                            <Link to='/' className='logo'>
                                <b className='logo-icon'>
                                    <img src={LogoIcon} alt='logo' />
                                </b>
                                <span className='logo-text'>
                                    <img src={LogoText} alt='homepage' />
                                </span>
                            </Link>
                        </div>
                        <a href='javascript:' className='topbartoggler d-block d-md-none' onClick={this.onClickShowProfile}>
                            <MoreHorizIcon />
                        </a>
                    </div>

                    <div className={`navbar-collapse collapse ${isShowTopHeaderProfile && 'show'}`}>
                        <ul className='navbar-nav float-left mr-auto' />
                        <ul className='navbar-nav float-right'>
                            <li className='nav-item dropdown'>
                                <a className='nav-link dropdown-toggle pro-pic' href='javascript:' onClick={this.onClickShowProfileDropdown}>
                                    <img src={LogoZyncIcon} alt='user' className='rounded-circle' width='40' />
                                    <span className='m-l-5 font-medium d-none d-sm-inline-block'>
                                        Jonathan Doe <FontAwesomeIcon icon='chevron-down' />
                                    </span>
                                </a>
                                <div className={`dropdown-menu dropdown-menu-right user-dd animated flipInY ${isShowProfileDropdown && 'show'}`}>
                                    <span className='with-arrow'>
                                        <span className='bg-primary' />
                                    </span>
                                    <div className='d-flex no-block align-items-center p-15 bg-primary text-white m-b-10'>
                                        <div>
                                            <img src={LogoZyncIcon} alt='user' className='rounded-circle' width='60' />
                                        </div>
                                        <div className='m-l-10'>
                                            <h4 className='m-b-0'>Jonathan Doe</h4>
                                            <p className='m-b-0'>jon@gmail.com</p>
                                        </div>
                                    </div>
                                    <div className='profile-dis'>
                                        <Link to='#' className='dropdown-item'>
                                            <FontAwesomeIcon icon='user' className='m-r-5 m-l-5' /> My Profile
                                        </Link>
                                        <Link to='#' className='dropdown-item'>
                                            <FontAwesomeIcon icon='power-off' className='m-r-5 m-l-5' /> Logout
                                        </Link>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
        )
    }
}

export default withRouter(TopBarHeader)
