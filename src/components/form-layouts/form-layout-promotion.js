import React, { Component } from 'react'
import { Field, reduxForm } from 'redux-form'
import InputField from '../forms/form-input'
import CKEditorField from '../forms/form-ckeditor'
import { Button } from 'reactstrap'
import { required } from 'redux-form-validators'

class FormPromotion extends Component {
    handleSubmit = (value) => {
        this.props.onFormSubmit(value, this.props.reset)
    }

    render () {
        const {
            reset,
            pristine,
            submitting,
            handleSubmit
        } = this.props
        return (
            <form onSubmit={handleSubmit(this.handleSubmit)}>
                <Field component={InputField} label='ชื่อโปรโมชั่น' name='name' validate={required()} />
                <Field component={CKEditorField} label='รายละเอียด' name='detail' />
                <Button type='submit' color='info' disabled={pristine || submitting}>บันทึก</Button>
                <Button color='link' onClick={reset}>ล้าง</Button>
            </form>
        )
    }
}

export default reduxForm({
    form: 'promotion',
    enableReinitialize: true
})(FormPromotion)
