import React, { Component } from 'react'
import { Field, reduxForm } from 'redux-form'
import InputField from '../forms/form-input'
import { Button } from 'reactstrap'
import SelectField from '../forms/form-select'
import client from '../../utils/client'
import { API_GROUP_URL, API_PERMISSION_URL } from '../../utils/urls'
import CheckboxField from '../forms/form-checkbox'
import { required } from 'redux-form-validators'

class FormUser extends Component {
    constructor (props) {
        super(props)
        this.state = {
            groups: [],
            permissions: []
        }
    }

    componentDidMount () {
        this.fetchGroups()
        this.fetchPermissions()
    }

    fetchGroups = () => {
        client.get(API_GROUP_URL).then(res => {
            this.setState({
                groups: res.data
            })
        })
    }

    fetchPermissions = () => {
        client.get(API_PERMISSION_URL).then(res => {
            this.setState({
                permissions: res.data
            })
        })
    }

    handleSubmit = (value) => {
        this.props.handleSubmitUser(value, this.props.reset)
    }

    getOptionLabel = option => option.name
    getOptionValue = option => option.id

    render () {
        const {
            reset,
            pristine,
            submitting,
            handleSubmit,
            editUser
        } = this.props

        const {
            groups,
            permissions
        } = this.state
        return (
            <form onSubmit={handleSubmit(this.handleSubmit)}>
                {!editUser && <Field component={InputField} label='Username' name='username' validate={required()} />}
                {!editUser && <Field component={InputField} type='password' label='รหัสผ่าน' name='password' validate={required()} />}
                <Field component={InputField} label='ชื่อ' name='first_name' validate={required()} />
                <Field component={InputField} label='นามสกุล' name='last_name' validate={required()} />
                <Field component={SelectField} label='กลุ่มพนักงาน' name='group' options={groups} getOptionLabel={this.getOptionLabel} getOptionValue={this.getOptionValue} isClearable />
                <Field component={SelectField} label='สิทธิ์การใช้งาน' name='permissions' options={permissions} getOptionLabel={this.getOptionLabel} getOptionValue={this.getOptionValue} isMulti isClearable />
                <Field component={CheckboxField} label='สถานะแอดมิน' name='is_staff' />
                <Button type='submit' color='info' disabled={pristine || submitting}>Save</Button>
                <Button color='link' onClick={reset} >ล้าง</Button>
            </form>
        )
    }
}

export default reduxForm({
    form: 'user',
    enableReinitialize: true
})(FormUser)
