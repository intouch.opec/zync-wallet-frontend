import React, { Component } from 'react'
import { Field, FieldArray, reduxForm } from 'redux-form'
import client from '../../utils/client'
import { API_BANK_URL, API_CUSTOMER_LEVEL_URL, API_PROMOTION_URL } from '../../utils/urls'
import InputField from '../forms/form-input'
import SelectField from '../forms/form-select'
import { Button } from 'reactstrap'
import { length, required } from 'redux-form-validators'
import AddIcon from '@material-ui/icons/Add'
import ClearIcon from '@material-ui/icons/Clear'
import { connect } from 'react-redux'
import { getBankIcon } from '../../utils/helpers'

class FormCustomer extends Component {
    constructor (props) {
        super(props)
        this.state = {
            banks: [],
            promotions: [],
            customerLevels: []
        }
    }

    componentDidMount () {
        this.fetchBanks()
        this.fetchPromotions()
        this.fetchCustomerLevels()
    }

    fetchBanks = () => {
        client.get(API_BANK_URL).then(res => {
            this.setState({
                banks: res.data
            })
        })
    }

    fetchPromotions = () => {
        client.get(API_PROMOTION_URL).then(res => {
            this.setState({
                promotions: res.data
            })
        })
    }

    fetchCustomerLevels = () => {
        client.get(API_CUSTOMER_LEVEL_URL).then(res => {
            this.setState({
                customerLevels: res.data
            })
        })
    }

    handleSubmit = (value) => {
        this.props.handleSubmitCustomer(value, this.props.reset)
    }

    getOptionValue = option => option.id
    getOptionLabel = option => option.name
    getOptionBankLabel = option => <div>{getBankIcon(option.code)} {option.name}</div>

    renderBankFields = ({ fields }) => {
        const {
            banks
        } = this.state

        return (
            <div>
                {fields.map((field, index) => (
                    <div key={index} className='row'>
                        <div className='col'>
                            <Field component={SelectField} label='ธนาคาร' name={`${field}.bank`} validate={required()} options={banks} getOptionLabel={this.getOptionBankLabel} getOptionValue={this.getOptionValue} />
                        </div>
                        <div className='col'>
                            <Field component={InputField} label='เลขที่บัญชีธนาคาร' name={`${field}.bank_no`} validate={required()} />
                        </div>
                        <div className='col-1 d-flex justify-content-center align-items-center'>
                            {index !== 0 &&
                                <Button color='link' className='text-danger' onClick={() => fields.remove(index)}>
                                    <ClearIcon />
                                </Button>
                            }
                        </div>
                    </div>
                ))}
                {fields.length < 3 &&
                    <div className='text-right'>
                        <Button color='link' onClick={() => fields.push({})}>
                            <AddIcon /> เพิ่มธนาคาร
                        </Button>
                    </div>
                }
            </div>
        )
    }

    render () {
        const {
            reset,
            pristine,
            submitting,
            handleSubmit,
            initialValues
        } = this.props

        const {
            promotions,
            customerLevels
        } = this.state

        const isEdit = initialValues.id != null
        return (
            <form onSubmit={handleSubmit(this.handleSubmit)}>
                <Field component={InputField} label='Username' name='username' validate={required()} disabled={isEdit} />
                {!isEdit && <Field component={InputField} type='password' label='Password' name='password' validate={required()} />}
                <Field component={InputField} label='ชื่อ' name='first_name' validate={required()} />
                <Field component={InputField} label='นามสกุล' name='last_name' validate={required()} />
                <Field component={InputField} label='อีเมล' name='email' validate={required()} />
                <Field component={InputField} label='เบอร​์โทรศัพท์' name='telephone' validate={required()} />
                <Field component={InputField} label='Line ID' name='line_id' validate={required()} />
                <Field component={SelectField} label='ระดับสมาชิก' name='customer_level' validate={required()} options={customerLevels} getOptionLabel={this.getOptionLabel} getOptionValue={this.getOptionValue} />
                <Field component={SelectField} label='โปรโมชั่น' name='promotions' options={promotions} getOptionLabel={this.getOptionLabel} getOptionValue={this.getOptionValue} isMulti isClearable />
                <FieldArray name='bank_accounts' component={this.renderBankFields} validate={[required(), length({ minimum: 1 })]} />
                <Button type='submit' color='info' disabled={pristine || submitting}>บันทึก</Button>
                <Button color='link' onClick={reset}>ล้าง</Button>
            </form>
        )
    }
}

const reduxFormCustomer = reduxForm({
    form: 'customer',
    enableReinitialize: true
})(FormCustomer)

const mapStateToProps = (state, ownProps) => {
    let initialValues = ownProps.initialValues
    if (!initialValues) {
        initialValues = {
            bank_accounts: [{}],
            customer_level: 1
        }
    }
    return {
        initialValues,
        currentValues: initialValues
    }
}

export default connect(mapStateToProps)(reduxFormCustomer)
