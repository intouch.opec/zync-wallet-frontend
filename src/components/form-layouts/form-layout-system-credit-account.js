import React, { Component } from 'react'
import { Field, reduxForm } from 'redux-form'
import client from '../../utils/client'
import { API_GAME_URL } from '../../utils/urls'
import InputField from '../forms/form-input'
import SelectField from '../forms/form-select'
import { Button } from 'reactstrap'
import { numericality, required } from 'redux-form-validators'

class FormSystemCreditAccount extends Component {
    constructor (props) {
        super(props)
        this.state = {
            games: []
        }
    }

    componentDidMount () {
        this.fetchGames()
    }

    fetchGames = () => {
        client.get(API_GAME_URL).then(res => {
            this.setState({
                games: res.data
            })
        })
    }

    handleSubmit = (value) => {
        this.props.onFormSubmit(value, this.props.reset)
    }

    getOptionLabel = option => option.name
    getOptionValue = option => option.id

    render () {
        const {
            reset,
            pristine,
            submitting,
            handleSubmit
        } = this.props
        const {
            games
        } = this.state
        return (
            <form onSubmit={handleSubmit(this.handleSubmit)}>
                <Field component={SelectField} label='เกม' name='game' validate={required()} options={games} getOptionLabel={this.getOptionLabel} getOptionValue={this.getOptionValue} />
                <Field component={InputField} label='ชื่อบัญชี' name='name' validate={required()} />
                <Field component={InputField} type='number' label='จำนวนเงิน' name='balance' validate={[required(), numericality({ '>': 0 })]} />
                <Button type='submit' color='info' disabled={pristine || submitting}>บันทึก</Button>
                <Button color='link' onClick={reset}>ล้าง</Button>
            </form>
        )
    }
}

export default reduxForm({
    form: 'systemAccount',
    enableReinitialize: true
})(FormSystemCreditAccount)
