import React from 'react'
import { Link, withRouter } from 'react-router-dom'
import routes from '../utils/routes'
import { connect } from 'react-redux'
import { authLogout } from '../actions/authentication'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { isPermissive } from '../utils/helpers'
import PerfectScrollbar from 'react-perfect-scrollbar'

class Sidebar extends React.Component {
    constructor (props) {
        super(props)
        this.state = {
            expandedRouteId: null
        }
    }

    componentDidMount () {
        routes.map(route => {
            (route.subRoutes || []).map(subRoute => {
                if (this.activeRoute(subRoute.path).indexOf('active') > -1) {
                    this.setState({
                        expandedRouteId: route.id || null
                    })
                }
            })
        })
    }

    activeRoute = (routeName) => {
        return this.props.location.pathname === routeName ? 'sidebar-link active' : 'sidebar-link'
    }

    setRouteId = (expandedRouteId) => {
        this.setState({
            expandedRouteId
        })
    }

    onClickExpandMenu = (e, expandedRouteId) => {
        e.preventDefault()
        if (this.state.expandedRouteId === expandedRouteId) {
            expandedRouteId = null
        }
        this.setRouteId(expandedRouteId)
    }

    onClickLogout = (e) => {
        e.preventDefault()
        this.props.authLogout()
    }

    getRenderMenu = () => {
        const {
            auth
        } = this.props

        return routes.filter(item => item.isShowOnMenu === true && isPermissive(auth, item.requiredPermission))
            .sort((a, b) => a.ordering >= b.ordering ? 1 : -1)
            .map(route => {
                let isHasRouteActive = this.state.expandedRouteId === route.id
                const subRoutes = (route.subRoutes || []).filter(item => item.isShowOnMenu === true && isPermissive(auth, item.requiredPermission))
                    .sort((a, b) => a.ordering >= b.ordering ? 1 : -1)
                    .map(subRoute => {
                        return (
                            <li key={subRoute.path} className='sidebar-item'>
                                <Link to={subRoute.path} className={this.activeRoute(subRoute.path)} onClick={() => { this.setRouteId(route.id) }}>
                                    {subRoute.icon}
                                    <span>{subRoute.name}</span>
                                </Link>
                            </li>
                        )
                    })

                if (subRoutes.length > 0) {
                    return (
                        <li key={route.name} className='sidebar-item'>
                            <a className={`sidebar-link has-arrow ${isHasRouteActive ? 'active' : ''}`} href='#' onClick={(e) => { this.onClickExpandMenu(e, route.id) }}>
                                {route.icon}
                                <span>{route.name}</span>
                            </a>
                            <ul className={`collapse first-level ${isHasRouteActive ? 'in' : ''}`}>
                                {subRoutes}
                            </ul>
                        </li>
                    )
                } else {
                    return (
                        <li key={route.path} className='sidebar-item'>
                            <Link to={route.path} className={this.activeRoute(route.path)} onClick={() => { this.setRouteId(route.id) }}>
                                {route.icon}
                                <span>{route.name}</span>
                            </Link>
                        </li>
                    )
                }
            })
    }

    render () {
        return (
            <aside className='left-sidebar'>
                <PerfectScrollbar className='scroll-sidebar'>
                    <nav className='sidebar-nav'>
                        <ul id='sidebarnav'>
                            {this.getRenderMenu()}
                            <li className='sidebar-item'>
                                <a href='#' className='sidebar-link' onClick={this.onClickLogout}>
                                    <FontAwesomeIcon icon='power-off' /> ออกจากระบบ
                                </a>
                            </li>
                        </ul>
                    </nav>
                </PerfectScrollbar>
            </aside>
        )
    }
}

function mapStateToProps ({ auth }) {
    return {
        auth
    }
}

export default withRouter(
    connect(mapStateToProps, {
        authLogout
    })(Sidebar)
)
