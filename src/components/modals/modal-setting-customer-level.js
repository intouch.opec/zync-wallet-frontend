import React, { Component } from 'react'
import { Field, reduxForm } from 'redux-form'
import { withRouter } from 'react-router-dom'
import Swal from 'sweetalert2'
import client from '../../utils/client'
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap'
import InputField from '../../components/forms/form-input'
import { required } from 'redux-form-validators'

class ModalSettingCustomerLevel extends Component {
    toggle = () => {
        this.props.toggle()
    }

    onClosed = () => {
        this.props.reset()
    }

    handleSubmit = (value) => {
        let req
        if (value.id) {
            req = client.put
        } else {
            req = client.post
        }
        req(this.props.apiEndpoint, value).then(res => {
            Swal.fire({
                type: 'success',
                title: 'บันทึกข้อมูลสำเร็จ',
                showConfirmButton: false,
                timer: 1500
            })
            this.props.onSubmitCallback(res)
        })
    }

    render () {
        const {
            pristine,
            submitting,
            handleSubmit,
            isModalOpen,
            initialValues
        } = this.props

        return (
            <Modal isOpen={isModalOpen} toggle={this.toggle} onClosed={this.onClosed}>
                <ModalHeader>{initialValues ? 'แก้ไขระดับลูกค้า' : 'เพิ่มระดับลูกค้าใหม่'}</ModalHeader>
                <form onSubmit={handleSubmit(this.handleSubmit)}>
                    <ModalBody>
                        <Field component={InputField} label='ชื่อระดับลูกค้า' name='name' validate={required()} />
                    </ModalBody>
                    <ModalFooter>
                        <Button color='link' onClick={this.toggle}>ยกเลิก</Button>
                        <Button type='submit' color='info' className='mr-1' disabled={pristine || submitting}>บันทึก</Button>
                    </ModalFooter>
                </form>
            </Modal>
        )
    }
}

const reduxFormModalSettingCustomerLevel = reduxForm({
    form: 'modalSettingCustomerLevel',
    enableReinitialize: true
})(ModalSettingCustomerLevel)

export default withRouter(reduxFormModalSettingCustomerLevel)
