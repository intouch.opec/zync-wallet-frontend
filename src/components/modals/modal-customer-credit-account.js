import React, { Component } from 'react'
import { Field, reduxForm } from 'redux-form'
import Swal from 'sweetalert2'
import client from '../../utils/client'
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap'
import InputField from '../../components/forms/form-input'
import { required } from 'redux-form-validators'
import SelectField from '../forms/form-select'
import { API_GAME_URL } from '../../utils/urls'

class ModalCustomerCreditAccount extends Component {
    constructor (props) {
        super(props)
        this.state = {
            games: []
        }
    }

    componentDidMount () {
        this.fetchGames()
    }

    fetchGames = () => {
        const {
            customerId
        } = this.props
        client.get(API_GAME_URL, { params: { customerId, filterType: 'customerCreditAccount' } }).then(res => {
            this.setState({
                games: res.data
            })
        })
    }
    toggle = () => {
        this.props.toggle()
    }

    onClosed = () => {
        this.props.reset()
    }

    handleSubmit = (value) => {
        const {
            customer,
            apiEndpoint
        } = this.props
        let req
        if (value.id) {
            req = client.put
        } else {
            req = client.post
        }
        req(apiEndpoint, { ...value, customer }).then(res => {
            Swal.fire({
                type: 'success',
                title: 'บันทึกข้อมูลสำเร็จ',
                showConfirmButton: false,
                timer: 1500
            })
            this.props.onSubmitCallback(res)
        })
    }

    getOptionLabel = option => option.name
    getOptionValue = option => option.id

    render () {
        const {
            pristine,
            submitting,
            handleSubmit,
            isModalOpen,
            initialValues
        } = this.props

        const {
            games
        } = this.state

        return (
            <Modal isOpen={isModalOpen} toggle={this.toggle} onClosed={this.onClosed}>
                <ModalHeader>{initialValues ? 'แก้ไขบัญชีเครดิต' : 'เพิ่มบัญชีเครดิต'}</ModalHeader>
                <form onSubmit={handleSubmit(this.handleSubmit)}>
                    <ModalBody>
                        <Field component={SelectField} label='เกม' name='game' validate={required()} options={games} getOptionLabel={this.getOptionLabel} getOptionValue={this.getOptionValue} disabled={initialValues === null} />
                        <Field component={InputField} label='Username' name='username' validate={required()} />
                        <Field component={InputField} label='Password' name='password' validate={required()} />
                    </ModalBody>
                    <ModalFooter>
                        <Button color='link' onClick={this.toggle}>ยกเลิก</Button>
                        <Button type='submit' color='info' className='mr-1' disabled={pristine || submitting}>บันทึก</Button>
                    </ModalFooter>
                </form>
            </Modal>
        )
    }
}

export default reduxForm({
    form: 'modalSettingGame',
    enableReinitialize: true
})(ModalCustomerCreditAccount)
