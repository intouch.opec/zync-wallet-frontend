import React, { Component } from 'react'
import { Modal, ModalBody, ModalHeader } from 'reactstrap'
import { getBankIcon, getDateFormat, getTransactionState, thousandFormat } from '../../utils/helpers'
import { API_URL } from '../../utils/urls'
import LayoutTransactionHistory from '../layouts/layout-transaction-history'
class ModalTransactionBankDetail extends Component {
    toggle = () => {
        this.props.toggle()
    }

    render () {
        const {
            isModalOpen,
            transactionBank
        } = this.props

        if (transactionBank === null) {
            return ''
        }

        return (
            <Modal isOpen={isModalOpen} toggle={this.toggle} size='lg' >
                <ModalHeader toggle={this.toggle}>ธุรกรรมการเงินเลขที่ {transactionBank.id}</ModalHeader>
                <ModalBody>
                    <div className='row mb-2'>
                        <div className='col-md-7'>
                            <h3 >รายละเอียดการทำธุรกรรม {getTransactionState(transactionBank.state)}</h3>
                            <hr />
                            <div className='row mb-2'>
                                <div className='col-md-6'>
                                    <h5 className='font-weight-bold'>บัญชีธนาคาร</h5>
                                    <h6>{getBankIcon(transactionBank.bank_account.bank.code)} {transactionBank.bank_account.account_no}</h6>
                                </div>
                                <div className='col-md-6'>
                                    <h5 className='font-weight-bold'>จำนวนเงิน</h5>
                                    <h6>{thousandFormat(transactionBank.amount)}</h6>
                                </div>
                                <div className='col-md-6'>
                                    <h5 className='font-weight-bold'>วันเวลาที่ทำรายการ</h5>
                                    <h6>{getDateFormat(transactionBank.transaction_at)}</h6>
                                </div>
                            </div>

                            <h3 >รายละเอียดลูกค้า</h3>
                            <hr />
                            <div className='row'>
                                <div className='col-md-6'>
                                    <h5 className='font-weight-bold'>ชื่อลูกค้า</h5>
                                    <h6>{transactionBank.customer.fullname}</h6>
                                </div>
                                <div className='col-md-6'>
                                    <h5 className='font-weight-bold'>Line ID</h5>
                                    <h6>{transactionBank.customer.line_id}</h6>
                                </div>
                                <div className='col-md-6'>
                                    <h5 className='font-weight-bold'>Email</h5>
                                    <h6>{transactionBank.customer.email}</h6>
                                </div>
                                <div className='col-md-6'>
                                    <h5 className='font-weight-bold'>เบอร์โทรศัพท์</h5>
                                    <h6>{transactionBank.customer.telephone}</h6>
                                </div>
                                <div className='col-md-6'>
                                    <h5 className='font-weight-bold'>ระดับลูกค้า</h5>
                                    <h6>{transactionBank.customer.customer_level.name}</h6>
                                </div>
                            </div>
                            <h5 className='font-weight-bold'>หมายเหตุ</h5>
                            <h6>{transactionBank.note}</h6>
                        </div>
                        <div className='col-md-5'>
                            {transactionBank.slip && <img src={`${API_URL}${transactionBank.slip}`} alt={transactionBank.slip} className='img-fluid' />}
                        </div>
                    </div>
                    <LayoutTransactionHistory transactions={transactionBank.transaction_history} />
                </ModalBody>
            </Modal>
        )
    }
}

export default ModalTransactionBankDetail
