import React from 'react'
import PreloaderIcon from '../../assets/images/preloader.svg'

class Preloader extends React.Component {
    render () {
        return (
            <div className='preloader'>
                <div className='loader-container'>
                    <PreloaderIcon fill='currentColor' stroke='currentColor' color='#188fa7' width={64} height={64} />
                </div>
            </div>
        )
    }
}

export default Preloader
