import React from 'react'

const Pagination = ({ items, previous, next, active, handleOnClick })=> {
    return (
        <ul className='pagination'>
            {
                previous === null && (
                <li className='paginate_button page-item previous disabled'>
                    <a onClick={() => handleOnClick(previous)} className='page-link'>Previous</a>
                </li>)
            }
            {items.map(value => {
                return  (
                <li key={value} className={`paginate_button page-item ${active === value && 'active'}`}>
                    <a onClick={() => handleOnClick(value)} className='page-link'>{value}</a>
                </li>
                )
            })}
            {
                next === null && (
                <li className='paginate_button page-item next' id='zero_config_next'>
                    <a onClick={() => handleOnClick(next)} className='page-link'>Next</a>
                </li>
                )
            }
    </ul>
    )
}

export default Pagination