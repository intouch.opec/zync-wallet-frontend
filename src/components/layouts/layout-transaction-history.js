import React from 'react'
import { getNoDataTableBody } from '../table'
import TableRow from '@material-ui/core/TableRow'
import TableCell from '@material-ui/core/TableCell'
import { getDateFormat, getTransactionState, getTransactionStatus, thousandFormat } from '../../utils/helpers'
import Table from '@material-ui/core/Table'
import TableHead from '@material-ui/core/TableHead'
import TableBody from '@material-ui/core/TableBody'

class LayoutTransactionHistory extends React.Component {
    constructor (props) {
        super(props)
        this.state = {
            isShowSideBar: false
        }
    }

    renderTableBody = () => {
        const {
            transactions
        } = this.props
        if (transactions.length === 0) return getNoDataTableBody(4)

        return transactions.map(row => {
            return (
                <TableRow key={row.id}>
                    <TableCell>{getDateFormat(row.created_at)}</TableCell>
                    <TableCell>{getTransactionState(row.state)}</TableCell>
                    <TableCell>{thousandFormat(row.amount)}</TableCell>
                    <TableCell>{getTransactionStatus(row.status)}</TableCell>
                </TableRow>
            )
        })
    }

    render () {
        return (
            <div className='table-responsive'>
                <h4 className='card-title'>ประวัติการทำรายการ</h4>
                <hr className='mb-0' />
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>วันที่ทำรายการ</TableCell>
                            <TableCell>ประเภทรายการ</TableCell>
                            <TableCell>จำนวนเงิน</TableCell>
                            <TableCell>สถานะ</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {this.renderTableBody()}
                    </TableBody>
                </Table>
            </div>
        )
    }
}

export default LayoutTransactionHistory
