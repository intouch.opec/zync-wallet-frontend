import React from 'react'
import { Card, CardBody } from 'reactstrap'
import LayoutTransactionHistory from './layout-transaction-history'

class LayoutTransactionCustomerDetail extends React.Component {
    render () {
        const {
            customerCreditAccount: { customer }
        } = this.props
        return (
            <div>
                <Card>
                    <CardBody>
                        <h4 className='card-title'>ข้อมูลลูกค้า</h4>
                        <hr />

                        <div className='row'>
                            <div className='col-md-6'>
                                <h5 className='font-weight-bold'>Username</h5>
                                <h6>{customer.username}</h6>
                            </div>
                            <div className='col-md-6'>
                                <h5 className='font-weight-bold'>Email</h5>
                                <h6>{customer.email}</h6>
                            </div>
                            <div className='col-md-6'>
                                <h5 className='font-weight-bold'>ชื่อลูกค้า</h5>
                                <h6>{customer.fullname}</h6>
                            </div>
                            <div className='col-md-6'>
                                <h5 className='font-weight-bold'>เบอร์โทรศัพท์</h5>
                                <h6>{customer.telephone}</h6>
                            </div>
                            <div className='col-md-6'>
                                <h5 className='font-weight-bold'>Line ID</h5>
                                <h6>{customer.line_id}</h6>
                            </div>
                            <div className='col-md-6'>
                                <h5 className='font-weight-bold'>ระดับลูกค้า</h5>
                                <h6>{customer.customer_level.name}</h6>
                            </div>
                        </div>
                    </CardBody>
                </Card>
                <Card className='mt-4'>
                    <CardBody>
                        <LayoutTransactionHistory transactions={customer.transaction_history} />
                    </CardBody>
                </Card>
            </div>
        )
    }
}

export default LayoutTransactionCustomerDetail
