import React from 'react'
import { Redirect, Route } from 'react-router-dom'
import { connect } from 'react-redux'

const UnauthorizedRoute = ({ component: Component, ...rest }) => {
    const getRenderComponent = (props) => {
        return window.localStorage.getItem('refreshToken') ? <Redirect to='/dashboard' /> : <Component {...props} />
    }

    return <Route {...rest} render={getRenderComponent} />
}

const mapStateToProps = ({ auth }) => {
    return {
        auth
    }
}

export default connect(mapStateToProps)(UnauthorizedRoute)
